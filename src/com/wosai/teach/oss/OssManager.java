package com.wosai.teach.oss;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import android.content.Context;
import android.graphics.Bitmap;

import com.alibaba.sdk.android.oss.OSSService;
import com.alibaba.sdk.android.oss.OSSServiceProvider;
import com.alibaba.sdk.android.oss.callback.SaveCallback;
import com.alibaba.sdk.android.oss.model.AccessControlList;
import com.alibaba.sdk.android.oss.model.ClientConfiguration;
import com.alibaba.sdk.android.oss.model.OSSException;
import com.alibaba.sdk.android.oss.model.TokenGenerator;
import com.alibaba.sdk.android.oss.storage.OSSBucket;
import com.alibaba.sdk.android.oss.storage.OSSData;
import com.alibaba.sdk.android.oss.util.OSSToolKit;

public class OssManager {
	private static OssManager ossManager;
	private OSSService ossService;

	/**
	 * 不考虑AK/SK的安全性
	 */
	final String accessKey = "vdJKU8a6iJXuZw63";
	final String screctKey = "2VbSryCuqGJfRggV5KddAvaxEnFlGS";

	/**
	 * 构造器
	 */
	private OssManager() {
		ossService = OSSServiceProvider.getService();
	}

	/**
	 * 获取OssManager的单例
	 */
	public static OssManager getInstance() {
		if (ossManager == null) {
			ossManager = new OssManager();
		}
		return ossManager;
	}

	/**
	 * 初始化
	 */
	public void init(Context context) {
		System.out.println("=========");
		System.out.println("===" + new Date() + "===");
		System.out.println("=========");
		ossService.setApplicationContext(context);
		ossService.setGlobalDefaultTokenGenerator(new TokenGenerator() {

			@Override
			public String generateToken(String httpMethod, String md5,
					String type, String date, String ossHeaders, String resource) {
				System.out.println("=========");
				System.out.println("===" + date + "===");
				System.out.println("=========");
				String content = httpMethod + "\n" + md5 + "\n" + type + "\n"
						+ date + "\n" + ossHeaders + resource;
				String token = OSSToolKit.generateToken(accessKey, screctKey,
						content);
				return token;
			}
		});
		// teach-platform.oss-cn-hangzhou.aliyuncs.com
		//
		ossService.setGlobalDefaultHostId("oss-cn-hangzhou.aliyuncs.com");
		ossService
				.setCustomStandardTimeWithEpochSec(System.currentTimeMillis() / 1000);
		ossService.setGlobalDefaultACL(AccessControlList.PUBLIC_READ); // 默认为private

		ClientConfiguration conf = new ClientConfiguration();
		conf.setConnectTimeout(15 * 1000); // 设置全局网络连接超时时间，默认30s
		conf.setSocketTimeout(15 * 1000); // 设置全局socket超时时间，默认30s
		conf.setMaxConnections(50); // 设置全局最大并发网络链接数, 默认50
		ossService.setClientConfiguration(conf);
	}

	// 异步上传数据
	public void asyncUpload(Bitmap bitmap, String filePath, String fileName,
			final OssCallBack callBack) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		byte[] dataToUpload = baos.toByteArray();
		OSSBucket bucket = ossService.getOssBucket("teach-platform");
		OSSData data = ossService.getOssData(bucket, filePath + fileName);
		data.setData(dataToUpload, "image/jpeg");
		data.uploadInBackground(new SaveCallback() {

			@Override
			public void onSuccess(String objectKey) {
				System.out.println("objectKey||" + objectKey);
				callBack.callBack(objectKey);
			}

			@Override
			public void onProgress(String objectKey, int byteCount,
					int totalSize) {
				System.out.println("objectKey||" + objectKey + "||byteCount||"
						+ byteCount + "||totalSize||" + totalSize);
			}

			@Override
			public void onFailure(String objectKey, OSSException ossException) {
				System.out.println("objectKey||" + objectKey
						+ "||ossException||");
				ossException.printStackTrace();
			}
		});
	}
}
