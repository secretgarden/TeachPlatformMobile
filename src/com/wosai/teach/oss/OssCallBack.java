package com.wosai.teach.oss;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-5-25 下午7:09:14
 * @desc : Oss回调函数
 */
public interface OssCallBack {
	/**
	 * 
	 */
	public void callBack(String str);
}
