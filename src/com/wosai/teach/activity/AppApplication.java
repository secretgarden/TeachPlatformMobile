package com.wosai.teach.activity;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;
import com.wosai.teach.oss.OssManager;
import com.wosai.teach.utils.ImageLoaders;

public class AppApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		// 初始化
		ActiveAndroid.initialize(this);
		ImageLoaders.getInstance().init(this);
		OssManager.getInstance().init(getApplicationContext());
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		// 销毁
		ActiveAndroid.dispose();
	}
}
