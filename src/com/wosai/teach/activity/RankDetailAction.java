package com.wosai.teach.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ListView;

import com.android.agnetty.constant.HttpCst;
import com.android.agnetty.core.AgnettyFutureListener;
import com.android.agnetty.core.AgnettyResult;
import com.android.agnetty.future.http.HttpFuture;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.wosai.teach.R;
import com.wosai.teach.adapter.RankAdapter;
import com.wosai.teach.handler.RankListHandler;
import com.wosai.teach.pojo.RankingDTO;
import com.wosai.teach.utils.AppGlobal;
import com.wosai.teach.utils.C;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年6月18日 下午3:01:36
 * @desc : 详细排名
 */
public class RankDetailAction extends BaseActivity {
	private int expId;
	private int pageSize = 10;
	private int curPage = 1;
	private String expName;
	private PullToRefreshListView rankList;
	private RankAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rank_dtl);
		enableBack(true);
		Intent it = getIntent();
		this.expId = it.getIntExtra("expId", 0);
		this.expName = it.getStringExtra("expName");
		System.out.println(this.expName);
		setTopTitle("详细排名");
		initView();
		loadSoftInfo();
	}

	public void initView() {
		rankList = (PullToRefreshListView) this
				.findViewById(R.id.rank_dtl_list);
		rankList.setMode(Mode.BOTH);
		rankList.setOnRefreshListener(new OnRefreshListener2<ListView>() {

			@Override
			public void onPullDownToRefresh(
					final PullToRefreshBase<ListView> refreshView) {
				System.out.println("下拉刷新" + refreshView.getClass());
				curPage = 1;
				loadSoftInfo();
				//结束加载
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				}, 1500);
			}

			@Override
			public void onPullUpToRefresh(
					final PullToRefreshBase<ListView> refreshView) {
				System.out.println("上拉加载" + refreshView.getClass());
				curPage++;
				loadSoftInfo();
				//结束加载
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				}, 1500);
				
			}
		});
	}

	/**
	 * @param rankList
	 * 
	 *            刷新显示排名
	 */
	private void refreshRankList(List<RankingDTO> rankDtoList) {
		if (adapter == null) {
			adapter = new RankAdapter(this, rankDtoList);
			rankList.setAdapter(adapter);
		} else if (curPage == 1) {
			adapter.setList(rankDtoList);
			adapter.notifyDataSetChanged();
		} else {
			adapter.addList(rankDtoList);
			adapter.notifyDataSetChanged();
		}
		rankList.onRefreshComplete();
	}

	private void loadSoftInfo() {
		String url = C.HTTP_URL_RANK_DTL_LIST;
		int userId = 0;
		if (AppGlobal.user != null) {
			userId = AppGlobal.user.getUserId();
		}
		url = url.replace("{userId}", userId + "");
		url = url.replace("{expId}", expId + "");
		url = url.replace("{pageSize}", this.pageSize + "");
		url = url.replace("{currentPage}", this.curPage + "");
		System.out.println(url);
		new HttpFuture.Builder(this, HttpCst.GET).setUrl(url)
				.setHandler(RankListHandler.class)
				.setListener(new AgnettyFutureListener() {
					@Override
					public void onComplete(AgnettyResult result) {
						super.onComplete(result);
						Object rst = result.getAttach();
						List<RankingDTO> rankList = new ArrayList<RankingDTO>();
						if (rst != null && rst instanceof List) {
							rankList = (List<RankingDTO>) rst;
						}
						refreshRankList(rankList);
					}

					@Override
					public void onException(AgnettyResult result) {
						super.onException(result);
					}
				}).execute();

	}

	@Override
	public void onResume() {
		super.onResume();
	}

}
