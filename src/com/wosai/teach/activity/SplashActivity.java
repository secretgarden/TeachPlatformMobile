package com.wosai.teach.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.android.agnetty.constant.HttpCst;
import com.android.agnetty.core.AgnettyFutureListener;
import com.android.agnetty.core.AgnettyResult;
import com.android.agnetty.future.http.HttpFuture;
import com.wosai.teach.R;
import com.wosai.teach.dialog.NewProgressDialog;
import com.wosai.teach.fragment.MySelfFragment;
import com.wosai.teach.handler.LoginHandler;
import com.wosai.teach.pojo.LogLogin;
import com.wosai.teach.utils.AppCst;
import com.wosai.teach.utils.AppGlobal;
import com.wosai.teach.utils.C;
import com.wosai.teach.utils.SharedSession;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		
//		Intent it = getIntent();
//		if (it != null && it.getFlags() == Intent.FLAG_ACTIVITY_CLEAR_TOP
//				&& it.getBooleanExtra("finish", false)) {
//			// 退出应用
//			System.exit(0);
//			finish();
//			return;
//		}

		// 第一次安装

		if (SharedSession.firstUse(this)) {
			Intent intent = new Intent(SplashActivity.this,
					WelcomActivity.class);
			startActivityForResult(intent, C.REQUEST_CODE_WELCOM);
			finish();
			return;
		} 
		
		/////////////////////////////
//		setContentView(R.layout.activity_splash);
		
		new Handler().postDelayed(new Runnable() {
			

			@Override
			public void run() {
				if (isFinishing()) {
					return;
				}
				
				final NewProgressDialog progressdialog;
				LogLogin logLogin;
				logLogin = new LogLogin();
				logLogin.init(SplashActivity.this);
				SharedPreferences sp;
				sp = getSharedPreferences("userInfo", 0);
		        boolean choseAutoLogin =sp.getBoolean("autologin", false);
		        String loginName=SharedSession.get(SplashActivity.this, "d_loginName");
		        String password=SharedSession.get(SplashActivity.this, "d_password");
				if(choseAutoLogin && MySelfFragment.getnoautologin()!=1 && !loginName.equals("") && !password.equals("")){
					progressdialog = NewProgressDialog.showProgress(SplashActivity.this, "登陆中...",
							false);
					String url = AppCst.HTTP_URL_LOGIN;
					url = url.replace("{loginName}", loginName + "")
							.replace("{password}", password + "")
							.replace("{imei}", logLogin.getImei() + "")
							.replace("{imsi}", logLogin.getImsi() + "")
							.replace("{appVer}", logLogin.getAppVer() + "")
							.replace("{osType}", logLogin.getOsType() + "")
							.replace("{osVer}", logLogin.getOsVer() + "")
							.replace("{manufacture}", logLogin.getManufacture() + "")
							.replace("{phoneModel}", logLogin.getPhoneModel() + "")
							.replace("{rom}", logLogin.getRom() + "")
							.replace("{ram}", logLogin.getRam() + "")
							.replace("{sd}", logLogin.getSd() + "")
							.replace("{freeRom}", logLogin.getFreeRom() + "")
							.replace("{freeRam}", logLogin.getFreeRam() + "")
							.replace("{freeSd}", logLogin.getFreeSd() + "")
							.replace("{screenX}", logLogin.getScreenX() + "")
							.replace("{screenY}", logLogin.getScreenY() + "")
							.replace("{network}", logLogin.getNetwork() + "");
					System.out.println(url);
					new HttpFuture.Builder(SplashActivity.this, HttpCst.GET).setUrl(url)
							.setHandler(LoginHandler.class)
							.setListener(new AgnettyFutureListener() {
								@Override
								public void onComplete(AgnettyResult result) {
									super.onComplete(result);
									Object rst = result.getAttach();
									if (rst != null && (Boolean) rst) {
										Toast.makeText(SplashActivity.this, "登录成功",
												Toast.LENGTH_LONG).show();
										AppGlobal.hasSchoolMate = false;
										Intent it = new Intent(SplashActivity.this,
												MainActivity.class);
//										it.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										startActivity(it);
										finish();
										if (progressdialog != null) {
											progressdialog.dismiss();
										}
									}else{
										Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
										startActivity(intent);
										finish();					
										if (progressdialog != null) {
											progressdialog.dismiss();
										}
										}
									}
								}).execute();
							}else{
								Intent intent = new Intent(SplashActivity.this,MainActivity.class);
								startActivity(intent);
								finish();								
							}
			}
		}, 1000);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == C.REQUEST_CODE_WELCOM) {
			Intent intent = new Intent(SplashActivity.this, MainActivity.class);
			startActivity(intent);
		}
	}

}
