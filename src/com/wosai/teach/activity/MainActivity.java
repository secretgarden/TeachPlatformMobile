package com.wosai.teach.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.SparseIntArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.agnetty.cache2.ImageCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.umeng.update.UmengUpdateAgent;
import com.wosai.teach.R;
import com.wosai.teach.utils.AppCst;
import com.wosai.teach.utils.AppGlobal;
import com.wosai.teach.utils.AppUtil;

public class MainActivity extends FragmentActivity {

	public static final int DIALOG_SYSTEM_MENU = 1;
	public static final int DIALOG_SYSTEM_MENU2 = 2;

	// 教学仿真大厅标签
	public static final String TAB_HALL = "HallTab";

	// 成绩
	public static final String TAB_REC = "RecTab";

	// 成就
	public static final String TAB_ACHIEVE = "AchieveTab";

	// 我的标签
	public static final String TAB_MYSELF = "MySelfTab";

	private static MyTabHost mTabhost;
	public static Activity myAct;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		UmengUpdateAgent.update(this);

		setContentView(R.layout.activity_main);

		myAct = this;

		mTabhost = (MyTabHost) findViewById(android.R.id.tabhost);
		mTabhost.setup();

		// 大厅
		mTabhost.addTab(mTabhost
				.newTabSpec(TAB_HALL)
				.setIndicator(
						newTabIndicator(R.string.main_index1,
								R.drawable.main_tab_hall))
				.setContent(R.id.tab1));
		// 成绩
		mTabhost.addTab(mTabhost
				.newTabSpec(TAB_REC)
				.setIndicator(
						newTabIndicator(R.string.main_index2,
								R.drawable.main_tab_rec)).setContent(R.id.tab2));
		// 成就
		mTabhost.addTab(mTabhost
				.newTabSpec(TAB_ACHIEVE)
				.setIndicator(
						newTabIndicator(R.string.main_index3,
								R.drawable.main_tab_achieve))
				.setContent(R.id.tab3));
		// 我的
		mTabhost.addTab(mTabhost
				.newTabSpec(TAB_MYSELF)
				.setIndicator(
						newTabIndicator(R.string.main_index4,
								R.drawable.main_tab_myself))
				.setContent(R.id.tab4));
		
		new Thread(new Runnable() {
		    public void run() {
		    	mTabhost.setCurrentTab(0);
		    }
		  }).start();
		
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		mTabhost = null;

		// 回收图片
		ImageCache.getInstance(this).clear();
		// 清除缓存图片
		ImageLoader.getInstance().clearMemoryCache();
	}

	/**
	 * 切换标签
	 * 
	 * @param tag
	 */
	public static void setCurrentTab(String tag) {
		if (mTabhost != null) {
			mTabhost.setCurrentTabByTag(tag);
		}
	}

	/**
	 * 
	 * @param resTxt
	 * @param resImg
	 * @return
	 */
	private View newTabIndicator(int resTxt, int resImg) {
		View indicator = LayoutInflater.from(this).inflate(
				R.layout.main_tab_indicator, null);
		ImageView icon = (ImageView) indicator
				.findViewById(R.id.main_tab_indicator_id_icon);
		icon.setImageDrawable(this.getResources().getDrawable(resImg));
		TextView text = (TextView) indicator
				.findViewById(R.id.main_tab_indicator_id_text);
		text.setText(resTxt);
		return indicator;
	}

	public class TabhostReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			if (AppCst.TAB_ACTION.equals(intent.getAction())) {

			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			AppUtil.AppOut(keyCode, event, this);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}
