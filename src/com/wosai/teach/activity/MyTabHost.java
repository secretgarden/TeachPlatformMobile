package com.wosai.teach.activity;

import com.wosai.teach.utils.AppUtil;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TabHost;

public class MyTabHost extends TabHost {
	public MyTabHost(Context context) {
		super(context);
	}

	public MyTabHost(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public MyTabHost(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
	}

	@Override
	public void setCurrentTabByTag(String tag) {
		if (MainActivity.TAB_MYSELF.equals(tag)
				|| MainActivity.TAB_REC.equals(tag)
				|| MainActivity.TAB_ACHIEVE.equals(tag)) {
			boolean toLogin = AppUtil.toLogin(MainActivity.myAct);
			if (toLogin) {
				return;
			}
		}
		super.setCurrentTabByTag(tag);
	}

	@Override
	public void setCurrentTab(int index) {
		// TODO index == 2
		if (index == 1 || index == 3 || index == 2) {
			boolean toLogin = AppUtil.toLogin(MainActivity.myAct);
			if (toLogin) {
				return;
			}
		}
		super.setCurrentTab(index);
	}

}
