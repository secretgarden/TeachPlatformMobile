package com.wosai.teach.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.wosai.teach.R;
import com.wosai.teach.adapter.NewPagerAdapter;
import com.wosai.teach.listener.NewPageChangeListener;

public class WelcomActivity extends Activity {

                 	private ViewGroup viewGroup;

	private ViewPager viewPager;

	private Button bt1;
//	private Button bt2;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		
		setContentView(R.layout.activity_welcom);
		initView();
		assignment();
	}

	public void initView() {
		viewPager = (ViewPager) this.findViewById(R.id.first_vp);
		viewGroup = (ViewGroup) this.findViewById(R.id.first_vg);

	}

	public void assignment() {
		LayoutInflater layIn = LayoutInflater.from(this);
		List<View> viewList = new ArrayList<View>();
		View v1 = layIn.inflate(R.layout.welcom_item_one, viewPager, false);
		View v2 = layIn.inflate(R.layout.welcom_item_two, viewPager, false);
//		View v3 = layIn.inflate(R.layout.welcom_item_one, viewPager, false);
//		View v4 = layIn.inflate(R.layout.welcom_item_two, viewPager, false);
		bt1 = (Button) v2.findViewById(R.id.first_bt1);
//		bt2 = (Button) v4.findViewById(R.id.first_bt1);
		viewList.add(v1);
		viewList.add(v2);
//		viewList.add(v3);
//		viewList.add(v4);
		NewPagerAdapter adapter = new NewPagerAdapter(viewList);
		viewPager.setAdapter(adapter);

		ImageView[] imageViews = new ImageView[viewList.size()];
		for (int i = 0; i < viewList.size(); i++) {
			ImageView imageView = new ImageView(this);
			imageView.setLayoutParams(new LayoutParams(36, 16));
			imageView.setPadding(10, 0, 10, 0);// 左 上 右 下
			imageViews[i] = imageView;
			if (i == 0) {
				// 默认选中第一张图片
				imageViews[i].setImageResource(R.drawable.point_white);
			} else {
				imageViews[i].setImageResource(R.drawable.point_grey);
			}
			viewGroup.addView(imageViews[i]);
		}
		final Activity activity = this;

		NewPageChangeListener listener = new NewPageChangeListener(imageViews);
		viewPager.setOnPageChangeListener(listener);

		bt1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// Intent intent = new Intent(activity, LoginActivity.class);
				Toast.makeText(WelcomActivity.this, "载入中，请稍后",
						Toast.LENGTH_LONG).show();
				Intent intent = new Intent(activity, MainActivity.class);
				startActivity(intent);
				finish();
			}
		});

	}
}
