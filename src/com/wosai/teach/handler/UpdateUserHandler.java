package com.wosai.teach.handler;

import java.util.LinkedHashMap;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.android.agnetty.constant.CharsetCst;
import com.android.agnetty.core.event.ExceptionEvent;
import com.android.agnetty.core.event.MessageEvent;
import com.android.agnetty.future.http.HttpHandler;
import com.android.agnetty.utils.HttpUtil;
import com.android.agnetty.utils.LogUtil;
import com.wosai.teach.dao.ExperimentDao;
import com.wosai.teach.pojo.Experiment;
import com.wosai.teach.pojo.JsonResult;
import com.wosai.teach.pojo.User;
import com.wosai.teach.utils.AppCst;
import com.wosai.teach.utils.GodUtils;

public class UpdateUserHandler extends HttpHandler {

	public UpdateUserHandler(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCompress(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		System.out.println("test11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
//		this.
		return false;
	}

	@Override
	public boolean onDecode(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onDecompress(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onEncode(MessageEvent evt) throws Exception {
		// TODO Auto-generated method stub
		 String dd=(String) evt.getData();

		LinkedHashMap<String, String> paramsMap = new LinkedHashMap<String, String>();

		paramsMap.put("content", dd);

		String params = HttpUtil.joinParamsWithEncode(paramsMap,
				CharsetCst.UTF_8);

		evt.setData(params.getBytes(CharsetCst.UTF_8));
		return false;
	}

	@Override
	public void onHandle(MessageEvent evt) throws Exception {
		String response = new String((byte[]) evt.getData(), "utf-8");
		LogUtil.d(LoginHandler.class.getCanonicalName() + response);
		if (!TextUtils.isEmpty(response)) {
			System.out.println(response);
//			JsonResult jr = JSON.parseObject(response, JsonResult.class);
			User userinfo = JSON.parseObject(response, User.class); 
				evt.getFuture().commitComplete(true);
		}
	}

	@Override
	public void onDispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onException(ExceptionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
