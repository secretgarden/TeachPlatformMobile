package com.wosai.teach.handler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.android.agnetty.core.event.ExceptionEvent;
import com.android.agnetty.core.event.MessageEvent;
import com.android.agnetty.future.http.HttpHandler;
import com.android.agnetty.utils.LogUtil;
import com.wosai.teach.pojo.HonorDTO;
import com.wosai.teach.pojo.JsonResult;
import com.wosai.teach.pojo.RankingDTO;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年6月17日 下午4:31:31
 * @desc : 排名列表
 */
public class RankListHandler extends HttpHandler {

	public RankListHandler(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCompress(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onDecode(MessageEvent evt) throws Exception {
		return false;
	}

	@Override
	public boolean onDecompress(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onEncode(MessageEvent evt) throws Exception {
		return false;
	}

	@Override
	public void onHandle(MessageEvent evt) throws Exception {
		String response = new String((byte[]) evt.getData(), "utf-8");
		LogUtil.d(LoginHandler.class.getCanonicalName() + response);
		if (!TextUtils.isEmpty(response)) {
			System.out.println(response);
			JsonResult jr = JSON.parseObject(response, JsonResult.class);
			List<RankingDTO> list = null;
			if (jr.getResult() == 0) {
				list = JSON.parseArray(jr.getObject().toString(),
						RankingDTO.class);
				evt.getFuture().commitComplete(list);
			} else {
				evt.getFuture().commitComplete(list);
			}
		}
	}

	@Override
	public void onDispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onException(ExceptionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
