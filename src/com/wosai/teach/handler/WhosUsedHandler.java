package com.wosai.teach.handler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.android.agnetty.core.event.ExceptionEvent;
import com.android.agnetty.core.event.MessageEvent;
import com.android.agnetty.future.http.HttpHandler;
import com.android.agnetty.utils.LogUtil;
import com.wosai.teach.pojo.Experiment;
import com.wosai.teach.pojo.JsonResult;
import com.wosai.teach.pojo.WhoUsed;

public class WhosUsedHandler extends HttpHandler {

	public WhosUsedHandler(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCompress(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onDecode(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onDecompress(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onEncode(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onHandle(MessageEvent evt) throws Exception {
		String response = new String((byte[]) evt.getData(), "utf-8");
		LogUtil.d(LoginHandler.class.getCanonicalName() + response);
		if (!TextUtils.isEmpty(response)) {
			System.out.println(response);
			JsonResult jr = JSON.parseObject(response, JsonResult.class);
			List<WhoUsed> wuList = null;
			if (jr.getResult() == 0) {
				JSONArray jsonArray = JSON
						.parseArray(jr.getObject().toString());
				wuList = new ArrayList<WhoUsed>();
				if (jsonArray != null) {
					Iterator<Object> it = jsonArray.iterator();
					while (it.hasNext()) {
						JSON json = (JSON) it.next();
						WhoUsed wu = JSON.parseObject(json.toString(),
								WhoUsed.class);
						wuList.add(wu);
					}
				}
				evt.getFuture().commitComplete(wuList);
			} else {
				evt.getFuture().commitComplete(wuList);
			}
		}
	}

	@Override
	public void onDispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onException(ExceptionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
