package com.wosai.teach.handler;

import android.os.Handler;
import android.os.Message;

import com.wosai.teach.activity.ExpDetailAction;
import com.wosai.teach.fragment.HallFragment;

public class InstallHandler extends Handler {

	public static int INSTALL_RESULT_OK = 0;

	public static int INSTALL_RESULT_ERR = 1;

	/**
	 * 当前的activity
	 */
	private Object curAct;

	public void setCurAct(Object curAct) {
		this.curAct = curAct;
	}

	@Override
	public void handleMessage(Message msg) {
		if (curAct == null) {
			return;
		} else if (curAct instanceof ExpDetailAction) {
			ExpDetailAction cur = (ExpDetailAction) curAct;
			cur.initView();
		} else if (curAct instanceof HallFragment) {
			HallFragment cur = (HallFragment) curAct;
			cur.getAdapter().notifyDataSetChanged();
		}

		if (msg.what == INSTALL_RESULT_ERR) {

		} else if (msg.what == INSTALL_RESULT_OK) {

		}

	}
}
