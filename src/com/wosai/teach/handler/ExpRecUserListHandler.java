package com.wosai.teach.handler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.android.agnetty.core.event.ExceptionEvent;
import com.android.agnetty.core.event.MessageEvent;
import com.android.agnetty.future.http.HttpHandler;
import com.android.agnetty.utils.LogUtil;
import com.wosai.teach.dao.ExpRecUserDao;
import com.wosai.teach.pojo.ExpRecUser;
import com.wosai.teach.pojo.JsonResult;

public class ExpRecUserListHandler extends HttpHandler {

	private int curPage = 0;

	public ExpRecUserListHandler(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean onCompress(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onDecode(MessageEvent evt) throws Exception {
		return false;
	}

	@Override
	public boolean onDecompress(MessageEvent arg0) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onEncode(MessageEvent evt) throws Exception {
		curPage = (Integer) evt.getData();
		return false;
	}

	@Override
	public void onHandle(MessageEvent evt) throws Exception {
		String response = new String((byte[]) evt.getData(), "utf-8");
		LogUtil.d(LoginHandler.class.getCanonicalName() + response);
		if (!TextUtils.isEmpty(response)) {
			System.out.println(response);
			JsonResult jr = JSON.parseObject(response, JsonResult.class);
			List<ExpRecUser> expList = null;
			if (jr.getResult() == 0) {
				System.out.println("当前页||" + curPage);
				expList = new ArrayList<ExpRecUser>();
				JSONArray jsonArray = JSON
						.parseArray(jr.getObject().toString());
				jr.getCurRec();
				if (jsonArray != null) {
					Iterator<Object> it = jsonArray.iterator();
					ExpRecUserDao expRecDao = new ExpRecUserDao();
					while (it.hasNext()) {
						JSON json = (JSON) it.next();
						ExpRecUser exp = JSON.parseObject(json.toString(),
								ExpRecUser.class);
						if (curPage == 1) {
							// 首页的数据存到数据库
							expRecDao.saveOrUpdate(exp);
						}
						expList.add(exp);
					}
				}
				evt.getFuture().commitComplete(expList);
			} else {
				evt.getFuture().commitComplete(expList);
			}
		}
	}

	@Override
	public void onDispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onException(ExceptionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
