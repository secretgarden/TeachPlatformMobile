package com.wosai.teach.adapter;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.agnetty.constant.HttpCst;
import com.android.agnetty.core.AgnettyFutureListener;
import com.android.agnetty.core.AgnettyResult;
import com.android.agnetty.future.http.HttpFuture;
import com.wosai.teach.R;
import com.wosai.teach.fragment.AchieveFragment;
import com.wosai.teach.handler.RankListHandler;
import com.wosai.teach.pojo.HonorDTO;
import com.wosai.teach.pojo.MedalDTO;
import com.wosai.teach.pojo.RankingDTO;
import com.wosai.teach.utils.AppCst;
import com.wosai.teach.utils.AppGlobal;
import com.wosai.teach.utils.GodUtils;
import com.wosai.teach.utils.ImageLoaders;
import com.wosai.teach.utils.ListViewUtils;
import com.wosai.teach.view.NoScrollListView;

/**
 * @author qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015年6月17日 上午9:45:18
 * @desc :
 */
public class AchieveAdapter extends BaseAdapter {
	private Activity mActivity;
	private AchieveFragment fragment;
	private LayoutInflater mInflater;
	private List<HonorDTO> list;
	private ListView listView;
	private int cur = 0;
	private int rankHeight = 0;
	private SparseIntArray sa = new SparseIntArray();
	private boolean showFlag = false;

	public AchieveAdapter(Activity activity, AchieveFragment fragment,
			List<HonorDTO> list) {
		this.mActivity = activity;
		this.fragment = fragment;
		this.mInflater = LayoutInflater.from(activity);
		this.list = list;
	}

	public boolean isCur(int cur) {
		if (this.cur == cur) {
			return true;
		} else {
			return false;
		}
	}

	public void setListView(ListView listView) {
		this.listView = listView;
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	public void setList(List<HonorDTO> list) {
		this.list = list;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.achieve_list_item, null);
			holder = new ViewHolder();
			holder.mIcon = (ImageView) convertView
					.findViewById(R.id.achieve_icon);
			holder.mName = (TextView) convertView
					.findViewById(R.id.achieve_name);
			holder.mGold = (ImageView) convertView
					.findViewById(R.id.medal_gold);
			holder.mSilver = (ImageView) convertView
					.findViewById(R.id.medal_silver);
			holder.mList = (NoScrollListView) convertView
					.findViewById(R.id.rank_list);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		HonorDTO honor = list.get(position);
		ImageLoaders.getInstance().displayImage(holder.mIcon,
				honor.getExpIcon1(), R.drawable.login_default_head);
		holder.mName.setText(honor.getExpName());
		holder.mGold.setImageResource(R.drawable.medal_gold1);
		holder.mSilver.setImageResource(R.drawable.medal_silver1);
		List<MedalDTO> medalList = honor.getListMedalDto();
		if (!GodUtils.CheckNull(medalList)) {
			for (MedalDTO mdeal : medalList) {
				if (mdeal.getLevel() == 1 && mdeal.getHonRecActive() == 1) {
					holder.mGold.setImageResource(R.drawable.medal_gold2);
				} else if (mdeal.getLevel() == 0
						&& mdeal.getHonRecActive() == 1) {
					holder.mSilver.setImageResource(R.drawable.medal_silver2);
				}
			}
		}
		if (!showFlag) {
			showRank(0);
		}
		return convertView;
	}

	/**
	 * 展示排名
	 */
	public void showRank(int position) {
		hiddenRank();
		cur = position;
		View view = listView.getChildAt(cur);
		if (view == null) {
			return;
		}
		showFlag = true;
		ViewHolder vh = (ViewHolder) view.getTag();
		vh.mList.setVisibility(View.VISIBLE);
		if (vh.mList.getAdapter() != null&&cur<0) {
			return;
		}

		HonorDTO honor = list.get(cur);
		String url = AppCst.HTTP_URL_RANK_LIST;
		int userId = 0;
		if (AppGlobal.user != null) {
			userId = AppGlobal.user.getUserId();
		}
		url = url.replace("{userId}", userId + "");
		url = url.replace("{expId}", honor.getExpId() + "");
		url = url.replace("{topX}", 3 + "");
		url = url.replace("{beforYAndMe}", 2 + "");
		url = url.replace("{afterZ}", 1 + "");
		System.out.println(url);

		new HttpFuture.Builder(mActivity, HttpCst.GET).setUrl(url)
				.setHandler(RankListHandler.class)
				.setListener(new AgnettyFutureListener() {
					@Override
					public void onComplete(AgnettyResult result) {
						super.onComplete(result);
						Object rst = result.getAttach();
						List<RankingDTO> rankList = new ArrayList<RankingDTO>();
						if (rst != null && rst instanceof List) {
							rankList = (List<RankingDTO>) rst;
						}
						refreshRankList(rankList);
					}

					@Override
					public void onException(AgnettyResult result) {
						super.onException(result);
					}
				}).execute();
	}

	/**
	 * @param rankList
	 * 
	 *            刷新显示排名
	 */
	private void refreshRankList(List<RankingDTO> rankList) {
		if (GodUtils.CheckNull(rankList)) {
			return;
		}
		View view = listView.getChildAt(cur);
		if (view == null) {
			return;
		}
		ViewHolder vh = (ViewHolder) view.getTag();
		vh.mList.setVisibility(View.VISIBLE);
//		if (vh.mList.getAdapter() == null) {
			RankAdapter rankAdapter = new RankAdapter(mActivity, rankList);
			vh.mList.setAdapter(rankAdapter);
//		}
		ListViewUtils.setListViewHeightBasedOnChildren(vh.mList);
		int rankHeight = vh.mList.getLayoutParams().height;
		ListViewUtils.setListViewHeightBasedOnChildren(listView);
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = params.height + rankHeight;
		listView.setLayoutParams(params);
	}

	/**
	 * 刷新高度
	 */
	public void refreshHeight() {
		ListViewUtils.setListViewHeightBasedOnChildren(listView);
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		System.out.println("|当前cur|" + sa.get(cur) + "|params.height|"
				+ params.height);
		params.height = params.height + sa.get(cur);
		listView.setLayoutParams(params);
	}

	/**
	 * 隐藏排名
	 */
	private void hiddenRank() {
		View view = listView.getChildAt(cur);
		if (view == null) {
			return;
		}
		ViewHolder vh = (ViewHolder) view.getTag();
		vh.mList.setVisibility(View.GONE);
	}

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	public class ViewHolder {
		// icon
		public ImageView mIcon;
		// 标题
		public TextView mName;

		public ImageView mGold;

		public ImageView mSilver;

		public NoScrollListView mList;
	}

}
