package com.wosai.teach.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.pojo.HomeWork;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午2:04:10
 * @desc :
 */
public class HomeWorkAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<HomeWork> list;

	public HomeWorkAdapter(Activity activity, List<HomeWork> list) {
		this.mInflater = LayoutInflater.from(activity);
		this.list = list;
	}

	public void setList(List<HomeWork> list) {
		this.list = list;
	}

	public void addList(List<HomeWork> list) {
		this.list.addAll(list);
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.home_work_item, null);
			holder = new ViewHolder();
			holder.hw_exp_name = (TextView) convertView
					.findViewById(R.id.hw_exp_name);
			holder.hw_msg = (TextView) convertView.findViewById(R.id.hw_msg);
			holder.hw_teach_name = (TextView) convertView
					.findViewById(R.id.hw_teach_name);
			holder.hw_dead_time = (TextView) convertView
					.findViewById(R.id.hw_dead_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		HomeWork hw = list.get(position);
		holder.hw_exp_name.setText("实验名称：" + hw.getExpName());
		holder.hw_teach_name.setText("        老师：" + hw.getTeacherName());
		holder.hw_dead_time.setText("完成时间：" + hw.getDeadtime());
		holder.hw_msg.setText("老师留言：" + hw.getMsg());
		return convertView;
	}

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	class ViewHolder {
		public TextView hw_exp_name;

		public TextView hw_msg;

		public TextView hw_teach_name;

		public TextView hw_dead_time;
	}

}
