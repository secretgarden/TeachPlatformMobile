package com.wosai.teach.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.pojo.WhoUsed;
import com.wosai.teach.utils.ImageLoaders;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午2:04:10
 * @desc :
 */
public class WhoUsedAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<WhoUsed> list;

	public WhoUsedAdapter(Activity activity, List<WhoUsed> list) {
		this.mInflater = LayoutInflater.from(activity);
		this.list = list;
	}

	public void setList(List<WhoUsed> list) {
		this.list = list;
	}

	public void addList(List<WhoUsed> list) {
		this.list.addAll(list);
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.who_used_item, null);
			holder = new ViewHolder();
			holder.icon = (ImageView) convertView
					.findViewById(R.id.who_used_icon);
			holder.name = (TextView) convertView
					.findViewById(R.id.who_used_name);
			holder.msg = (TextView) convertView.findViewById(R.id.who_used_msg);
			holder.time = (TextView) convertView
					.findViewById(R.id.who_used_time);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		WhoUsed hw = list.get(position);
		StringBuffer show = new StringBuffer();
		show.append("操作" + hw.getExpName());
		show.append(hw.getMsg());
		ImageLoaders.getInstance().displayRoundImage(holder.icon,
				hw.getIcon1(), R.drawable.login_default_head);
		holder.name.setText(hw.getShowName());
		holder.msg.setText(show.toString());
		holder.time.setText(hw.getTime());
		return convertView;
	}

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	class ViewHolder {
		public ImageView icon;
		public TextView name;
		public TextView msg;
		public TextView time;
	}

}
