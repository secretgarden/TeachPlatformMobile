package com.wosai.teach.adapter;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.fragment.HallFragment;
import com.wosai.teach.pojo.Experiment;
import com.wosai.teach.pojo.Install;
import com.wosai.teach.utils.AppGlobal;
import com.wosai.teach.utils.AppUtil;
import com.wosai.teach.utils.C;
import com.wosai.teach.utils.ImageLoaders;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午2:04:10
 * @desc :
 */
public class HallAdapter extends BaseAdapter {
	private Activity mActivity;
	private HallFragment fragment;
	private LayoutInflater mInflater;
	private List<Experiment> expList;

	public HallAdapter(Activity activity, HallFragment fragment,
			List<Experiment> expList) {
		this.mActivity = activity;
		this.fragment = fragment;
		this.mInflater = LayoutInflater.from(activity);
		this.expList = expList;
	}

	@Override
	public int getCount() {
		return expList == null ? 0 : expList.size();
	}

	@Override
	public Object getItem(int position) {
		return expList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public boolean checkDown(int position) {
		Experiment exp = expList.get(position);
		String tt[] = exp.getApkURL().split("/");
		String fileName = tt[tt.length - 1];
		String sdcard = Environment.getExternalStorageDirectory().getPath();
		
		String savePath = sdcard + "/teach_platform/" + fileName;
//		String savePath = sdcard + "/teach_platform/";
		
		File file = new File(savePath);
		if (file.exists()) {
			Install ins=new Install();
			ins.setSavePath(savePath);
			ins.save();
			return true;
		}else{
			return false;
			}
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.teach_list_item, null);
			holder = new ViewHolder();
			holder.mLlRoot = (LinearLayout) convertView
					.findViewById(R.id.msg_list_id_root);
			holder.mIcon = (ImageView) convertView
					.findViewById(R.id.teach_list_icon);
			holder.mTxtTitle = (TextView) convertView
					.findViewById(R.id.msg_list_id_title);
			holder.mTxtDesc = (TextView) convertView
					.findViewById(R.id.msg_list_id_desc);
			holder.pbar = (ProgressBar) convertView.findViewById(R.id.down_pb);
			holder.pbPercent = (TextView) convertView
					.findViewById(R.id.pb_percent);
			holder.teachButton = (Button) convertView
					.findViewById(R.id.teach_list_bt);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		Experiment exp = expList.get(position);
		exp.initNative(mActivity);
		// 标题
		holder.mTxtTitle.setText(exp.getExpName());
		// 描述
		holder.mTxtDesc.setText("大小：" + exp.getApkSize());
		ImageLoaders.getInstance().displayImage(holder.mIcon, exp.getIcon1(),
				R.drawable.icon);

		holder.pbPercent.setVisibility(View.GONE);
		holder.pbar.setVisibility(View.GONE);
			
		if (exp.getVersionCode() > 0) {
			// 已安装
			holder.teachButton.setText("打开");
			exp.setStatus(C.TEACH_STATUS_INSTALL);
		} else if (checkDown(position)
				&& exp.getStatus() == C.TEACH_STATUS_LOADOVER
				) {
			// 已下载
			holder.teachButton.setText("安装");
			exp.setStatus(C.TEACH_STATUS_LOADOVER);
		} else if (exp.getStatus() == C.TEACH_STATUS_DOWNLOAD) {
			// 正在下载
			holder.teachButton.setText("下载中");
			holder.pbPercent.setVisibility(View.VISIBLE);
			holder.pbar.setVisibility(View.VISIBLE);
			AppGlobal.factory.download(holder, exp);
		} else {
			// 未安装
			holder.teachButton.setText("下载");
			exp.setStatus(C.TEACH_STATUS_NOT_INSTALL);
		}
		exp.save();
		HallListener listener = new HallListener(exp, holder);
		holder.teachButton.setOnClickListener(listener);
		return convertView;
	}

	class HallListener implements OnClickListener {
		private Experiment exp;
		private ViewHolder holder;

		HallListener(Experiment exp, ViewHolder holder) {
			this.exp = exp;
			this.holder = holder;
		}

		@Override
		public void onClick(View v) {
			boolean toLogin = AppUtil.toLogin(mActivity);
			if (toLogin) {
				return;
			}
			switch (exp.getStatus()) {
			case C.TEACH_STATUS_INSTALL:
				// 打开
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_LAUNCHER);
				ComponentName cn = new ComponentName(exp.getApkPackageName(),
						exp.getApkClassName());
				intent.setComponent(cn);
				// intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.putExtra("userId", AppGlobal.user.getUserId());
				// http://blog.csdn.net/berber78/article/details/7278408
				fragment.startActivityForResult(intent, C.REQUEST_CODE_OPEN);
				break;
			case C.TEACH_STATUS_LOADOVER:
				AppUtil.install(fragment, exp.getExpId());
				break;
			case C.TEACH_STATUS_NOT_INSTALL:
				AppGlobal.factory
						.checkDownLoad(exp, holder, holder.teachButton);
				break;
			default:
				break;
			}
		}
	};

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	public class ViewHolder {
		public LinearLayout mLlRoot;
		// icon
		public ImageView mIcon;
		// 标题
		public TextView mTxtTitle;
		// 描述
		public TextView mTxtDesc;

		public ProgressBar pbar;

		public TextView pbPercent;

		public Button teachButton;
	}

}
