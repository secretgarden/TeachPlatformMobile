package com.wosai.teach.adapter;

import java.util.List;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wosai.teach.R;
import com.wosai.teach.dao.ExperimentDao;
import com.wosai.teach.pojo.ExpRecUser;
import com.wosai.teach.pojo.Experiment;
import com.wosai.teach.utils.ImageLoaders;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午2:04:10
 * @desc :
 */
public class RecAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	private List<ExpRecUser> list;

	public RecAdapter(Activity activity, Fragment fragment,
			List<ExpRecUser> list) {
		this.mInflater = LayoutInflater.from(activity);
		this.list = list;
	}

	public void setList(List<ExpRecUser> list) {
		this.list = list;
	}

	public void addList(List<ExpRecUser> list) {
		this.list.addAll(list);
	}

	@Override
	public int getCount() {
		return list == null ? 0 : list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.exp_rec_item, null);
			holder = new ViewHolder();
			holder.mIcon = (ImageView) convertView.findViewById(R.id.exp_icon);
			holder.name = (TextView) convertView
					.findViewById(R.id.exp_rec_name);
			holder.score = (TextView) convertView
					.findViewById(R.id.exp_rec_score);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		ExpRecUser exp = list.get(position);
		holder.name.setText(exp.getExpName());
		holder.name.setTextSize(14);
		holder.score.setText(exp.getScore() + "分");
		holder.score.setTextSize(14);
		
		ExperimentDao dao = new ExperimentDao();
		Experiment expt = new Experiment();
		if (exp.getExpId() != null) {
			expt = dao.getExperimentByExpId(exp.getExpId());
		}
		ImageLoaders.getInstance().displayImage(holder.mIcon, expt.getIcon1(),
				R.drawable.icon);
		return convertView;
	}

	/**
	 * @author : qiumy
	 * @e-mail : qiumy@wosaitech.com
	 * @date : 2015-4-2 上午10:35:43
	 * @desc :
	 */
	class ViewHolder {
		// icon
		public ImageView mIcon;
		// 标题
		public TextView name;
		// 描述
		public TextView score;

	}

}
