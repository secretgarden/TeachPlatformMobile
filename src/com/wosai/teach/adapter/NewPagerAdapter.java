package com.wosai.teach.adapter;

import java.util.List;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

public class NewPagerAdapter extends PagerAdapter {
	public List<View> viewList;

	public NewPagerAdapter(List<View> viewList) {
		this.viewList = viewList;
	}

	@Override
	public int getCount() {
		 return viewList.size();
		// 设置成最大，使用户看不到边界
//		return Integer.MAX_VALUE;
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == arg1;
	}

	/**
	 */
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		/*-
		 View v = viewList.get(position);
		 container.addView(v);
		 return v;
		 */
		// 对ViewPager页号求模取出View列表中要显示的项
		position %= viewList.size();
		if (position < 0) {
			position = viewList.size() + position;
		}
		View view = viewList.get(position);
		// 如果View已经在之前添加到了一个父组件，则必须先remove，否则会抛出IllegalStateException。
		ViewParent vp = view.getParent();
		if (vp != null) {
			ViewGroup parent = (ViewGroup) vp;
			parent.removeView(view);
		}
		container.addView(view);
		// add listeners here if necessary
		return view;
	}

	/**
	 */
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Warning：不要在这里调用removeView
		// container.removeView(viewList.get(position));
	}

}
