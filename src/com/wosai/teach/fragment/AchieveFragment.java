package com.wosai.teach.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.agnetty.constant.HttpCst;
import com.android.agnetty.core.AgnettyFutureListener;
import com.android.agnetty.core.AgnettyResult;
import com.android.agnetty.future.http.HttpFuture;
import com.wosai.teach.R;
import com.wosai.teach.activity.RankDetailAction;
import com.wosai.teach.adapter.AchieveAdapter;
import com.wosai.teach.handler.HonorListHandler;
import com.wosai.teach.pojo.HonorDTO;
import com.wosai.teach.pojo.User;
import com.wosai.teach.utils.AppCst;
import com.wosai.teach.utils.AppGlobal;
import com.wosai.teach.view.NoScrollListView;

/**
 * @author qiumy * @e-mail : qiumy@wosaitech.com
 * @date : 2015年6月16日 下午1:54:32
 * @desc : 成就列表
 */
public class AchieveFragment extends Fragment {
	private List<HonorDTO> honorList;
	private View view;

	private User user;

	private NoScrollListView listMsg;
	private AchieveAdapter adapter;
	private RelativeLayout rl;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.frag_achieve, null);
		user = AppGlobal.user;
		if (user != null) {
			initView(view);
			loadHonor();
		}
		return view;
	}

	private void initView(View view) {
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText("成就");
		this.rl = (RelativeLayout) view.findViewById(R.id.achieve_rl);
		listMsg = (NoScrollListView) view.findViewById(R.id.achieve_list);
		listMsg.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if (adapter.isCur(position)) {
					// 进入详细排名
					Intent it = new Intent(getActivity(),
							RankDetailAction.class);
					HonorDTO honor = honorList.get(position);
					it.putExtra("expId", honor.getExpId());
					it.putExtra("expName", honor.getExpName());
					startActivity(it);
				} else {
					// 查看简略排名
					adapter.showRank(position);
				}

			}
		});
	}

	private void refreshList() {
		if (adapter == null) {
			adapter = new AchieveAdapter(getActivity(), this, honorList);
			listMsg.setAdapter(adapter);
			adapter.setListView(listMsg);
		} else {
			adapter.setList(honorList);
			adapter.notifyDataSetChanged();
		}
		adapter.refreshHeight();
	}

	private void loadHonor() {
		String url = AppCst.HTTP_URL_HONOR_LIST;
		int userId = 0;
		if (AppGlobal.user != null) {
			userId = AppGlobal.user.getUserId();
		}
		url = url.replace("{userId}", userId + "");
		System.out.println(url);
		new HttpFuture.Builder(getActivity(), HttpCst.GET).setUrl(url)
				.setHandler(HonorListHandler.class)
				.setListener(new AgnettyFutureListener() {
					@Override
					public void onComplete(AgnettyResult result) {
						super.onComplete(result);
						Object rst = result.getAttach();
						if (rst != null && rst instanceof List) {
							honorList = (List<HonorDTO>) rst;
						} else {
							honorList = new ArrayList<HonorDTO>();
						}
						refreshList();
					}

					@Override
					public void onException(AgnettyResult result) {
						super.onException(result);
					}
				}).execute();

	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		adapter.notifyDataSetChanged();
	}
}
