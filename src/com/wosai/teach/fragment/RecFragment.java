package com.wosai.teach.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.agnetty.constant.HttpCst;
import com.android.agnetty.core.AgnettyFutureListener;
import com.android.agnetty.core.AgnettyResult;
import com.android.agnetty.future.http.HttpFuture;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.wosai.teach.R;
import com.wosai.teach.adapter.RecAdapter;
import com.wosai.teach.dao.ExpRecUserDao;
import com.wosai.teach.handler.ExpRecUserListHandler;
import com.wosai.teach.pojo.ExpRecUser;
import com.wosai.teach.pojo.User;
import com.wosai.teach.utils.AppCst;
import com.wosai.teach.utils.AppGlobal;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午4:41:16
 * @desc : 成绩列表
 */
public class RecFragment extends Fragment {
	private ExpRecUserDao expRecUserDao;
	private List<ExpRecUser> expList;
	private View view;

	private int pageSize = 10;

	private int curPage = 1;
	private User user;

	private PullToRefreshListView listMsg;
	private RecAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.frag_rec, null);
		user = AppGlobal.user;
		if (user != null) {
			initView(view);
			loadSoftInfo();
		}
		return view;
	}

	private void loadSoftInfo() {
		String url = AppCst.HTTP_URL_EXPERIMENT_REC_LIST;
		int userId = 0;
		if (AppGlobal.user != null) {
			userId = AppGlobal.user.getUserId();
		}
		url = url.replace("{userId}", userId + "");
		url = url.replace("{pageSize}", pageSize + "");
		url = url.replace("{curPage}", curPage + "");
		System.out.println(url);
		new HttpFuture.Builder(getActivity(), HttpCst.GET).setUrl(url)
				.setData(curPage).setHandler(ExpRecUserListHandler.class)
				.setListener(new AgnettyFutureListener() {
					@Override
					public void onComplete(AgnettyResult result) {
						super.onComplete(result);
						Object rst = result.getAttach();
						if (rst != null && rst instanceof List) {
							expList = (List<ExpRecUser>) rst;
						} else {
							expList = new ArrayList<ExpRecUser>();
						}
						refreshRecList();
					}

					@Override
					public void onException(AgnettyResult result) {
						super.onException(result);
						listMsg.onRefreshComplete();
					}
				}).execute();
	}

	private void initView(View view) {
		TextView title = (TextView) view.findViewById(R.id.title);
		title.setText("记录");
		listMsg = (PullToRefreshListView) view.findViewById(R.id.rec_list);
		listMsg.setMode(Mode.BOTH);
		expRecUserDao = new ExpRecUserDao();
		expList = expRecUserDao.findAllExpRecUser();
		adapter = new RecAdapter(getActivity(), this, expList);
		listMsg.setAdapter(adapter);

		listMsg.setOnRefreshListener(new OnRefreshListener2<ListView>() {

			@Override
			public void onPullDownToRefresh(
					final PullToRefreshBase<ListView> refreshView) {
				System.out.println("下拉刷新" + refreshView.getClass());
				curPage = 1;
				loadSoftInfo();
				
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				}, 1500);
			}

			@Override
			public void onPullUpToRefresh(
					final PullToRefreshBase<ListView> refreshView) {
				System.out.println("上拉加载" + refreshView.getClass());
				curPage++;
				loadSoftInfo();
				
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						refreshView.onRefreshComplete();
					}
				}, 1500);
			}
		});

		this.refreshRecList();
	}

	private void refreshRecList() {
		if (curPage == 1) {
			adapter.setList(expList);
		} else {
			adapter.addList(expList);
		}
		adapter.notifyDataSetChanged();
		listMsg.onRefreshComplete();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		adapter.notifyDataSetChanged();
	}
}
