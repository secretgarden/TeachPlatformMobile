package com.wosai.teach.pojo;

import java.io.Serializable;
import java.util.Date;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "ExperimentRecModel")
public class ExperimentRec extends Model implements Serializable {

	private static final long serialVersionUID = 1L;

	public ExperimentRec() {
	}

	public ExperimentRec(Integer userId, Integer expId, Integer isFinished,
			Integer lastStep, Integer timeCost, Integer score, Integer level,
			Date beginTime, Date endTime, String expVer) {
		this.userId = userId;
		this.expId = expId;
		this.isFinished = isFinished;
		this.endStep = lastStep;
		this.timeCost = timeCost;
		this.score = score;
		this.level = level;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.expVer = expVer;
	}

	@Column
	private Integer recId;

	@Column
	private Integer expId;

	@Column
	private Integer userId;

	@Column
	private Integer level;

	@Column
	private Integer endStep;

	@Column
	private Integer isFinished;

	@Column
	private Integer score;

	@Column
	private Integer timeCost;

	@Column
	private Date beginTime;

	@Column
	private Date endTime;

	@Column
	private String expVer;

	@Column
	private Integer totalStep; // 用户实际操作了多少次

	@Column
	private Integer rightStep; // 其中正确的次数

	public Integer getRecId() {
		return recId;
	}

	public void setRecId(Integer recId) {
		this.recId = recId;
	}

	public Integer getExpId() {
		return expId;
	}

	public void setExpId(Integer expId) {
		this.expId = expId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getExpLevel() {
		return level;
	}

	public void setExpLevel(Integer expLevel) {
		this.level = expLevel;
	}

	public Integer getEndStep() {
		return endStep;
	}

	public void setEndStep(Integer endStep) {
		this.endStep = endStep;
	}

	public Integer getTimeCost() {
		return timeCost;
	}

	public void setTimeCost(Integer timeCost) {
		this.timeCost = timeCost;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getExpVer() {
		return expVer;
	}

	public void setExpVer(String expVer) {
		this.expVer = expVer;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getIsFinished() {
		return isFinished;
	}

	public void setIsFinished(Integer isFinished) {
		this.isFinished = isFinished;
	}

	public Integer getTotalStep() {
		return totalStep;
	}

	public void setTotalStep(Integer totalStep) {
		this.totalStep = totalStep;
	}

	public Integer getRightStep() {
		return rightStep;
	}

	public void setRightStep(Integer rightStep) {
		this.rightStep = rightStep;
	}
}
