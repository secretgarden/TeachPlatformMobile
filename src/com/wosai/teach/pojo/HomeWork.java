package com.wosai.teach.pojo;

import java.io.Serializable;

import com.activeandroid.Model;

public class HomeWork extends Model implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer expId;

	private String expName;

	private String deadtime;// YYYY-MM-DD

	private String teacherName;

	private String msg;// 其他附加信息，比如老师的留言等。

	public Integer getExpId() {
		return expId;
	}

	public void setExpId(Integer expId) {
		this.expId = expId;
	}

	public String getExpName() {
		return expName;
	}

	public void setExpName(String expName) {
		this.expName = expName;
	}

	public String getDeadtime() {
		return deadtime;
	}

	public void setDeadtime(String deadtime) {
		this.deadtime = deadtime;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}