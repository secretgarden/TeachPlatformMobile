package com.wosai.teach.pojo;

import java.io.Serializable;
import java.util.Date;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-31 下午1:50:28
 * @desc : 教学仿真实体
 */
@Table(name = "TeachModel")
public class Teach extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column
	private String code;

	@Column
	private String name;

	@Column
	private Integer versionNo;

	@Column
	private Integer maxVersionNo;

	@Column
	private String version;

	@Column
	private String packageName;

	@Column
	private String className;

	/**
	 * 0,不可用，1未安装，2已安装，3安装中,9异常
	 */
	@Column
	private Integer status;

	@Column
	private Date createDate;

	@Column
	private String desc;
	
	@Column
	private String downloadUrl;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public Integer getMaxVersionNo() {
		return maxVersionNo;
	}

	public void setMaxVersionNo(Integer maxVersionNo) {
		this.maxVersionNo = maxVersionNo;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDownloadUrl() {
		return downloadUrl;
	}

	public void setDownloadUrl(String downloadUrl) {
		this.downloadUrl = downloadUrl;
	}

}
