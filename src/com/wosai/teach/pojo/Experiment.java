package com.wosai.teach.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.pm.PackageInfo;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-5-6 下午3:32:08
 * @desc :
 */
@Table(name = "ExperimentModel")
public class Experiment extends Model implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column
	private Integer expId;

	@Column
	private String expName;

	@Column
	private String webURL;

	@Column
	private String iosURL;

	@Column
	private String apkURL;

	@Column
	private long apkVer;

	@Column
	private String apkPackageName;

	@Column
	private String apkClassName;

	@Column
	private String h5URL;

	@Column
	private Date createTime;

	@Column
	private Integer isExpire;

	@Column
	private Date updateTime;

	@Column
	private Date expireTime;

	@Column
	private String apkSize;// 单位MB，一般保留两位小鼠。

	@Column
	private String icon1;

	@Column
	private Date tmApkUpdate;

	@Column
	private Integer popularity;

	@Column
	private String picURL1;

	@Column
	private String picURL2;

	@Column
	private String picURL3;

	@Column
	private String picURL4;

	@Column
	private String picURL5;

	@Column
	private String keywords;

	@Column
	private String detailInfo;

	/**
	 * 0,不可用，1未安装，2已安装，3安装中,9异常
	 */
	@Column
	private Integer status;
	/**
	 * native版本号
	 */
	private int versionCode;

	/**
	 * native版本名称
	 */
	private String versionName;

	public void initNative(Context context) {
		versionCode = 0;
		versionName = "";
		List<PackageInfo> packages = context.getPackageManager()
				.getInstalledPackages(0);
		for (int i = 0; i < packages.size(); i++) {
			PackageInfo packageInfo = packages.get(i);
			if (this.apkPackageName.equals(packageInfo.packageName)) {
				this.versionCode = packageInfo.versionCode;
				this.versionName = packageInfo.versionName;
			}
		}
	}

	public Integer getExpId() {
		return expId;
	}

	public void setExpId(Integer expId) {
		this.expId = expId;
	}

	public String getExpName() {
		return expName;
	}

	public void setExpName(String expName) {
		this.expName = expName;
	}

	public String getWebURL() {
		return webURL;
	}

	public void setWebURL(String webURL) {
		this.webURL = webURL;
	}

	public String getIosURL() {
		return iosURL;
	}

	public void setIosURL(String iosURL) {
		this.iosURL = iosURL;
	}

	public String getApkURL() {
		return apkURL;
	}

	public void setApkURL(String apkURL) {
		this.apkURL = apkURL;
	}

	public String getH5URL() {
		return h5URL;
	}

	public void setH5URL(String h5URL) {
		this.h5URL = h5URL;
	}

	public Integer getIsExpire() {
		return isExpire;
	}

	public void setIsExpire(Integer isExpire) {
		this.isExpire = isExpire;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	public Integer getStatus() {
		if (status == null) {
			status = -1;
		}
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public long getApkVer() {
		return apkVer;
	}

	public void setApkVer(long apkVer) {
		this.apkVer = apkVer;
	}

	public String getApkSize() {
		return apkSize;
	}

	public void setApkSize(String apkSize) {
		this.apkSize = apkSize;
	}

	public String getIcon1() {
		return icon1;
	}

	public void setIcon1(String icon1) {
		this.icon1 = icon1;
	}

	public Date getTmApkUpdate() {
		return tmApkUpdate;
	}

	public void setTmApkUpdate(Date tmApkUpdate) {
		this.tmApkUpdate = tmApkUpdate;
	}

	public Integer getPopularity() {
		return popularity;
	}

	public void setPopularity(Integer popularity) {
		this.popularity = popularity;
	}

	public String getPicURL1() {
		return picURL1;
	}

	public void setPicURL1(String picURL1) {
		this.picURL1 = picURL1;
	}

	public String getPicURL2() {
		return picURL2;
	}

	public void setPicURL2(String picURL2) {
		this.picURL2 = picURL2;
	}

	public String getPicURL3() {
		return picURL3;
	}

	public void setPicURL3(String picURL3) {
		this.picURL3 = picURL3;
	}

	public String getPicURL4() {
		return picURL4;
	}

	public void setPicURL4(String picURL4) {
		this.picURL4 = picURL4;
	}

	public String getPicURL5() {
		return picURL5;
	}

	public void setPicURL5(String picURL5) {
		this.picURL5 = picURL5;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getDetailInfo() {
		return detailInfo;
	}

	public void setDetailInfo(String detailInfo) {
		this.detailInfo = detailInfo;
	}

	public String getApkPackageName() {
		return apkPackageName;
	}

	public void setApkPackageName(String apkPackageName) {
		this.apkPackageName = apkPackageName;
	}

	public String getApkClassName() {
		return apkClassName;
	}

	public void setApkClassName(String apkClassName) {
		this.apkClassName = apkClassName;
	}

	public int getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(int versionCode) {
		this.versionCode = versionCode;
	}

	public String getVersionName() {
		return versionName;
	}

	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

}
