package com.wosai.teach.pojo;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.view.WindowManager;

import com.wosai.teach.R;
import com.wosai.teach.utils.C;
import com.wosai.teach.utils.GodUtils;
import com.wosai.teach.utils.MeneryInfo;
import com.wosai.teach.utils.NetWorkUtils;

public class LogLogin implements Serializable {

	public LogLogin() {
		return;
	}

	public LogLogin(Integer userId, String imei, String imsi, String appVer,
			Integer osType, String osVer, String manufacture,
			String phoneModel, Integer rom, Integer ram, Integer sd,
			Integer freeRom, Integer freeRam, Integer freeSd, Integer screenX,
			Integer screenY, Integer network) {
		this.userId = userId;
		this.imei = imei;
		this.imsi = imsi;
		this.appVer = appVer;
		this.osType = osType;
		this.osVer = osVer;
		this.manufacture = manufacture;
		this.phoneModel = phoneModel;
		this.rom = rom;
		this.ram = ram;
		this.sd = sd;
		this.freeRom = freeRom;
		this.freeRam = freeRam;
		this.freeSd = freeSd;
		this.screenX = screenX;
		this.screenY = screenY;
		this.network = network;
		this.loginTime = new Date();
		return;
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public void init(Context context) {
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		ActivityManager am = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		MemoryInfo mi = new MemoryInfo();
		am.getMemoryInfo(mi);
		// 唯一的设备ID
		this.setImei(tm.getDeviceId());
		// 唯一的用户ID
		this.setImsi(tm.getSubscriberId());
		// app版本号
		this.setAppVer(context.getResources().getString(R.string.version));
		this.setOsType(C.OS_TYPE_ANDROID);
		this.setOsVer(android.os.Build.VERSION.RELEASE);
		// 手机/电脑生产厂商，如apple
		this.setManufacture(Build.MANUFACTURER);
		// //手机、电脑型号，如iPhone6plus
		this.setPhoneModel(android.os.Build.MODEL);
		this.setRom(MeneryInfo.getRomTotalSize());
		if (Build.VERSION.SDK_INT >= 16) {
			this.setRam((int) (mi.totalMem / (1024 * 1024)));
		} else {
			this.setRam(-1);
		}
		this.setFreeRam((int) (mi.availMem / (1024 * 1024)));
		this.setSd(MeneryInfo.getSDTotalSize());
		this.setFreeRom(MeneryInfo.getRomAvailableSize());
		this.setFreeSd(MeneryInfo.getSDAvailableSize());
		if (Build.VERSION.SDK_INT >= 13) {
			Point size = new Point();
			wm.getDefaultDisplay().getSize(size);
			this.setScreenX(size.x);
			this.setScreenY(size.y);
		} else {
			this.setScreenX(wm.getDefaultDisplay().getWidth());
			this.setScreenY(wm.getDefaultDisplay().getHeight());
		}
		// 网络类型
		int netType = tm.getNetworkType();
		int netClassType = NetWorkUtils.getNetworkClassByType(netType);
		this.setNetwork(netClassType);
	}

	private static final long serialVersionUID = 1L;
	private Integer logId;

	private Integer userId;

	private Date loginTime;

	private String imsi;

	private String imei;

	private String appVer;

	private Integer osType;

	private String osVer;

	private String manufacture;

	private String phoneModel;

	private Integer rom;

	private Integer ram;

	private Integer sd;

	private Integer freeRom;

	private Integer freeRam;

	private Integer freeSd;

	private Integer screenX;

	private Integer screenY;

	private Integer network;

	public Integer getLogId() {
		return logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public String getImsi() {
		if (GodUtils.CheckNull(imsi)) {
			imsi = "Nan";
		}
		return imsi;
	}

	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getAppVer() {
		return appVer;
	}

	public void setAppVer(String appVer) {
		this.appVer = appVer;
	}

	public Integer getOsType() {
		return osType;
	}

	public void setOsType(Integer osType) {
		this.osType = osType;
	}

	public String getOsVer() {
		return osVer;
	}

	public void setOsVer(String osVer) {
		this.osVer = osVer;
	}

	public String getManufacture() {
		return manufacture;
	}

	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}

	public String getPhoneModel() {
		try {
			phoneModel = URLEncoder.encode(phoneModel, "utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return phoneModel;
	}

	public void setPhoneModel(String phoneModel) {
		this.phoneModel = phoneModel;
	}

	public Integer getRom() {
		return rom;
	}

	public void setRom(Integer rom) {
		this.rom = rom;
	}

	public Integer getRam() {
		return ram;
	}

	public void setRam(Integer ram) {
		this.ram = ram;
	}

	public Integer getSd() {
		return sd;
	}

	public void setSd(Integer sd) {
		this.sd = sd;
	}

	public Integer getFreeRom() {
		return freeRom;
	}

	public void setFreeRom(Integer freeRom) {
		this.freeRom = freeRom;
	}

	public Integer getFreeRam() {
		return freeRam;
	}

	public void setFreeRam(Integer freeRam) {
		this.freeRam = freeRam;
	}

	public Integer getFreeSd() {
		return freeSd;
	}

	public void setFreeSd(Integer freeSd) {
		this.freeSd = freeSd;
	}

	public Integer getScreenX() {
		return screenX;
	}

	public void setScreenX(Integer screenX) {
		this.screenX = screenX;
	}

	public Integer getScreenY() {
		return screenY;
	}

	public void setScreenY(Integer screenY) {
		this.screenY = screenY;
	}

	public Integer getNetwork() {
		return network;
	}

	public void setNetwork(Integer network) {
		this.network = network;
	}

}
