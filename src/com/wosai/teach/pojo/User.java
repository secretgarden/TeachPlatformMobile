package com.wosai.teach.pojo;

import java.io.Serializable;
import java.util.Date;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

@Table(name = "User")
public class User extends Model implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column
	private Integer userId;
	@Column
	private String loginName;
	@Column
	private Integer loginModel;
	@Column
	private String password;
	@Column
	private Date regTimestamp;
	@Column
	private String userName;
	@Column
	private String nickName;
	@Column
	private String oaId;
	@Column
	private Integer classId;
	@Column
	private String email;
	@Column
	private String microMsg;
	@Column
	private String qq;
	@Column
	private String mobile;
	@Column
	private Integer isAdmin;
	@Column
	private Integer isTeacher;
	@Column
	private Integer isStudent;
	@Column
	private Integer isExpire;
	@Column
	private Date updateTime;
	@Column
	private Date expireTime;

	@Column
	private String icon1;// 用户头像的URL

	@Column
	private String sex;// 性别，女；female,男；male

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public Integer getLoginModel() {
		return loginModel;
	}

	public void setLoginModel(Integer loginModel) {
		this.loginModel = loginModel;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getRegTimestamp() {
		return regTimestamp;
	}

	public void setRegTimestamp(Date regTimestamp) {
		this.regTimestamp = regTimestamp;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getOaId() {
		return oaId;
	}

	public void setOaId(String oaId) {
		this.oaId = oaId;
	}

	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMicroMsg() {
		return microMsg;
	}

	public void setMicroMsg(String microMsg) {
		this.microMsg = microMsg;
	}

	public String getQQ() {
		return qq;
	}

	public void setQQ(String qq) {
		this.qq = qq;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Integer isAdmin) {
		this.isAdmin = isAdmin;
	}

	public Integer getIsTeacher() {
		return isTeacher;
	}

	public void setIsTeacher(Integer isTeacher) {
		this.isTeacher = isTeacher;
	}

	public Integer getIsStudent() {
		return isStudent;
	}

	public void setIsStudent(Integer isStudent) {
		this.isStudent = isStudent;
	}

	public Integer getIsExpire() {
		return isExpire;
	}

	public void setIsExpire(Integer isExpire) {
		this.isExpire = isExpire;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Date getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getIcon1() {
		return icon1;
	}

	public void setIcon1(String icon1) {
		this.icon1 = icon1;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}
}
