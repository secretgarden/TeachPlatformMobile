package com.wosai.teach.pojo;

import java.io.Serializable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-5-8 下午4:05:41
 * @desc :
 */
@Table(name = "DownloadInfoModel")
public class DownloadInfo extends Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 创建一个下载信息的实体类
	 */
	@Column
	private int expId;
	@Column
	private int threadId;// 下载器id
	@Column
	private int startPos;// 开始点
	@Column
	private int endPos;// 结束点
	@Column
	private int compeleteSize;// 完成度
	@Column
	private String url;// 下载器网络标识

	public DownloadInfo(int threadId, int startPos, int endPos,
			int compeleteSize, String url, int expId) {
		this.threadId = threadId;
		this.startPos = startPos;
		this.endPos = endPos;
		this.compeleteSize = compeleteSize;
		this.url = url;
		this.expId = expId;
	}

	public DownloadInfo() {
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getThreadId() {
		return threadId;
	}

	public void setThreadId(int threadId) {
		this.threadId = threadId;
	}

	public int getStartPos() {
		return startPos;
	}

	public void setStartPos(int startPos) {
		this.startPos = startPos;
	}

	public int getEndPos() {
		return endPos;
	}

	public void setEndPos(int endPos) {
		this.endPos = endPos;
	}

	public int getCompeleteSize() {
		return compeleteSize;
	}

	public void setCompeleteSize(int compeleteSize) {
		this.compeleteSize = compeleteSize;
	}

	public void addCompeleteSize(int compeleteSize) {
		this.compeleteSize += compeleteSize;
	}

	public int getExpId() {
		return expId;
	}

	public void setExpId(int expId) {
		this.expId = expId;
	}

	@Override
	public String toString() {
		return "DownloadInfo [threadId=" + threadId + ", startPos=" + startPos
				+ ", endPos=" + endPos + ", compeleteSize=" + compeleteSize
				+ ", expId=" + expId + "]";
	}
}
