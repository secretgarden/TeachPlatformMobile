package com.wosai.teach.listener;

import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.widget.ImageView;

import com.wosai.teach.R;

public class NewPageChangeListener implements OnPageChangeListener {

	private ImageView[] imageViews;

	public NewPageChangeListener(ImageView[] imageViews) {
		this.imageViews = imageViews;
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {

	}

	/**
	 * 操作圆点轮换变背景
	 */
	@Override
	public void onPageSelected(int postion) {
		postion = postion % imageViews.length;
		for (int i = 0; i < imageViews.length; i++) {
			imageViews[postion].setImageResource(R.drawable.point_white);
			if (postion != i) {
				imageViews[i].setImageResource(R.drawable.point_grey);
			}
		}
	}

}
