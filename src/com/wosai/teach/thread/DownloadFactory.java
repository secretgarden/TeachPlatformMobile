package com.wosai.teach.thread;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.SparseArray;
import android.widget.Button;
import android.widget.Toast;

import com.wosai.teach.adapter.HallAdapter;
import com.wosai.teach.adapter.HallAdapter.ViewHolder;
import com.wosai.teach.handler.PBarHandler;
import com.wosai.teach.pojo.Experiment;
import com.wosai.teach.utils.C;
import com.wosai.teach.utils.NetWorkUtils;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-5-13 上午11:34:22
 * @desc : 管理下载线程
 */
public class DownloadFactory {
	private Activity mActivity;
	private SparseArray<DownloadRunnable> runnableSA;

	public DownloadFactory(Activity activity) {
		this.mActivity = activity;
		runnableSA = new SparseArray<DownloadRunnable>();
	}

	public boolean checkDownLoad(final Experiment exp, final ViewHolder holder,
			final Button button) {
		int netType = NetWorkUtils.getNetworkType(mActivity);
		if (netType == NetWorkUtils.NETTYPE_NO) {
			Toast.makeText(mActivity, "亲，请连接网络先", Toast.LENGTH_LONG).show();
			return false;
		}
		if (netType != NetWorkUtils.NETTYPE_WIFI) {
			new AlertDialog.Builder(mActivity)
					.setIcon(android.R.drawable.ic_dialog_info)
					.setMessage("您当前未连接WIFI，是否确认下载安装")
					.setPositiveButton("确定",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Toast.makeText(mActivity, "下载安装中",
											Toast.LENGTH_LONG).show();
									exp.setStatus(C.TEACH_STATUS_DOWNLOAD);
									button.setText("下载中");
									download(holder, exp);
								}
							})
					.setNegativeButton("取消",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// 点击“返回”后的操作,这里不设置没有任何操作
								}
							}).show();
		} else {
			Toast.makeText(mActivity, "下载安装中", Toast.LENGTH_LONG).show();
			exp.setStatus(C.TEACH_STATUS_DOWNLOAD);
			button.setText("下载中");
			download(holder, exp);
		}
		return true;
	}

	/**
	 * 
	 * 
	 * @param pbHandler
	 * @param exp
	 */
	public void download(HallAdapter.ViewHolder holder, Experiment exp) {
		// http://www.cnblogs.com/hanyonglu/archive/2012/02/20/2358801.html
		PBarHandler pbHandler = null;
		if (holder != null) {
			pbHandler = new PBarHandler(mActivity, holder.pbar,
					holder.pbPercent);
		}
		DownloadRunnable dr = runnableSA.get(exp.getExpId());
		if (dr == null) {
			DownloadRunnable dl = new DownloadRunnable(exp, mActivity,
					pbHandler, this);
			runnableSA.put(exp.getExpId(), dl);
			new Thread(dl).start();
		} else {
			dr.modifyPBarHandler(pbHandler);
		}
	}

	/**
	 * 杀死所有下载线程
	 */
	public void killAll() {
		if (runnableSA == null) {
			return;
		}
		int key = 0;
		for (int i = 0; i < runnableSA.size(); i++) {
			key = runnableSA.keyAt(i);
			DownloadRunnable dl = runnableSA.get(key);
			dl.setKillFlag(true);
		}
		runnableSA = new SparseArray<DownloadRunnable>();
	}

	/**
	 * 检查是否在下载
	 * 
	 * @param exp
	 * @return
	 */
	public boolean checkByExp(Experiment exp) {
		DownloadRunnable dr = runnableSA.get(exp.getExpId());
		if (dr == null) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 
	 */
	public void refresh() {

	}

	public SparseArray<DownloadRunnable> getRunnableSA() {
		return runnableSA;
	}
}
