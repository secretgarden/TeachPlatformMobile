/*
 * ========================================================
 * Copyright(c) 2012 杭州龙骞科技-版权所有
 * ========================================================
 * 本软件由杭州龙骞科技所有, 未经书面许可, 任何单位和个人不得以
 * 任何形式复制代码的部分或全部, 并以任何形式传播。
 * 公司网址
 * 
 * 			http://www.hzdracom.com/
 * 
 * ========================================================
 */

package com.wosai.teach.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

import com.android.agnetty.utils.DeviceUtil;

/**
 * @author : Zhenshui.Xia
 * @date   : 2014-4-15
 * @desc   : 
 */
public class BaseDialog extends Dialog{
	protected Context mContext;
	
	public BaseDialog(Context context) {
		super(context);
		this.mContext = context;
	}

	public BaseDialog(Context context, int theme){
	    super(context, theme);
	    this.mContext = context;
	}
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCanceledOnTouchOutside(false);
    }

	@Override
	public void show() {
		super.show();
		
		Window window = getWindow();        
		// 获取对话框当前的参数值
	    WindowManager.LayoutParams params = window.getAttributes(); 
	    //高度
	    params.height = LayoutParams.WRAP_CONTENT;   
	    //宽度设置为屏幕的0.8
	    params.width = (int) (DeviceUtil.getDevice(mContext).getWidth() * 0.9); ; 
	    window.setAttributes(params);
	}
	
	
	
}
