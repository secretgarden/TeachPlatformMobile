package com.wosai.teach.utils;

import android.os.Environment;

public class AppCst extends EnumCst {
	public static String APP_CHANNEL;
	// 应用版本
	public static String APP_VERSION;

	// ------------------------database--------------------------
	// 数据库名
	public final static String DB_NAME = "app.db";
	// 数据库版本
	public final static int DB_VERSION = 27;

	// app识别码
	public final static String ID_CODE = "BDEB65E65349BC005874FEF1C3A00201";

	// 是否是调试模式
	public static final boolean DEBUG = true;

	// 在百度开发者中心查询应用的API Key(推送)
	public final static String BAIDU_PUSH_API_KEY = "Yu1j7qj3MEfnBWgApvRssjhy";
	// 在百度开发者中心查询应用的API Key(定位)
	public final static String BAIDU_LOC_API_KEY = "Yu1j7qj3MEfnBWgApvRssjhy";

	public static final String MSG_ACTION = "com.android.inmotiontec.iparker.ACTION_MSG";
	public static final String TAB_ACTION = "com.android.inmotiontec.iparker.ACTION_TAB";

	/* umeng */
	public static final String DESCRIPTOR = "com.wosai.teach";

	private static final String TIPS = "请移步官方网站 ";
	private static final String END_TIPS = ", 查看相关说明.";
	public static final String TENCENT_OPEN_URL = TIPS
			+ "http://wiki.connect.qq.com/android_sdk使用说明" + END_TIPS;
	public static final String PERMISSION_URL = TIPS
			+ "http://wiki.connect.qq.com/openapi权限申请" + END_TIPS;

	public static final String SOCIAL_LINK = "http://www.umeng.com/social";
	public static final String SOCIAL_TITLE = "友盟社会化组件帮助应用快速整合分享功能";
	public static final String SOCIAL_IMAGE = "http://www.umeng.com/images/pic/banner_module_social.png";

	public static final String SOCIAL_CONTENT = "友盟社会化组件（SDK）让移动应用快速整合社交分享功能，我们简化了社交平台的接入，为开发者提供坚实的基础服务：（一）支持各大主流社交平台，"
			+ "（二）支持图片、文字、gif动图、音频、视频；@好友，关注官方微博等功能"
			+ "（三）提供详尽的后台用户社交行为分析。http://www.umeng.com/social";
	/* umeng */

	// API接口地址
	// public final static String HTTP_URL =
	// "http://121.40.195.52/TeachPlatform/";
	public final static String HTTP_URL = "http://121.40.195.52:8080/teach_platform/service/";
	// public final static String HTTP_URL =
	// "http://192.168.3.106:8080/teach_platform";

	public final static boolean HTTP_DEBUG = false;

	public final static long RESERVE_TIMEOUT_TIME = 15 * 60 * 1000;
	//用户登陆
	public final static String HTTP_URL_LOGIN = HTTP_URL
			+ "/appLogin/{loginName}/{password}/{imei}/{imsi}/{appVer}/{osType}/{osVer}/{manufacture}/{phoneModel}/{rom}/{ram}/{sd}/{freeRom}/{freeRam}/{freeSd}/{screenX}/{screenY}/{network}";
	//获取实验清单
	public final static String HTTP_URL_EXPERIMENT_LIST = HTTP_URL
			+ "/experiment/listAll/{userId}";
	//获取作业清单
	public final static String HTTP_URL_HOME_LIST = HTTP_URL
			+ "/homework/classmates/{userId}/{pageSize}/{currentPage}";
	//获取本人实验记录
	public final static String HTTP_URL_EXPERIMENT_REC_LIST = HTTP_URL
			+ "/expRec/myRec/{userId}/{pageSize}/{curPage}";
	
	//获取同班同学最近的实验记录
	///expRec/classmates/{userId}/{pageSize}/{currentPage}
	
	//上传成绩
	///expRec/upload/{userId}/{expVer}/{expId}/{isFinished}/{lastStep}/{score}/{level}/{beginTime}/{endTime}/{totalStep}/{rightStep}
	
	//获取系统下发的消息
	public final static String HTTP_URL_MESSAGE_LOGIN_HALL = HTTP_URL
			+ "/message/loginHall/{userId}/{pageSize}/{currentPage}";
	
	//获取大厅页面图片URL
	public final static String HTTP_URL_PIC_LOGIN_HALL = HTTP_URL
			+ "/pic/loginHall/{userId}/{pageSize}/{currentPage}";
	
	//获取大厅实验列表信息
	///exp/loginHall/{userId}/{pageSize}/{currentPage}
	public final static String HTTP_URL_WHO_USED = HTTP_URL
//			+"/exp/loginHall/{userId}/{pageSize}/{currentPage}";
			+ "/expRec/whoUsed/{expId}/{userId}/{pageSize}/{currentPage}";
			
	
	//获取实验详情
	public final static String HTTP_URL_EXP_DETAIL = HTTP_URL
			+ "/exp/detail/{expId}/{userId}";
	//修改用户资料
	public final static String HTTP_USER_INFO_UPDATE = HTTP_URL
			+"/user/info/update/{userId}";
	
	
	
	//获取个人勋章
	public final static String HTTP_URL_HONOR_LIST = HTTP_URL
			+ "/user/honor/{userId}";
	//获取排名概要
	public final static String HTTP_URL_RANK_LIST = HTTP_URL
			+ "/user/honor/ranking/overview/{userId}/{expId}/{topX}/{beforYAndMe}/{afterZ}";
	//获取详细排名
	public final static String HTTP_URL_RANK_DTL_LIST = HTTP_URL
			+ "/user/honor/ranking/detailed/{userId}/{expId}/{pageSize}/{currentPage}";

	public final static String HTTP_URL_MSG = "https://sandboxapp.cloopen.com:8883";

	public final static String OSS_URL = "http://teach-platform.oss-cn-hangzhou.aliyuncs.com/";

	public static final int TAG_MESSAGE_DELAYED = 100;

	public static final String MARQUEE_DIV = "                                                      ";

	public static final String SEX_BOY = "男";

	public static final String SEX_GIRL = "女";

	public static final String SEX_MALE = "male";

	public static final String SEX_FEMALE = "female";

	public static final String ACTION_REFRESH = "refresh";

	public static final int REQUEST_CODE_WELCOM = 17;

	public final static String SAVE_HEAD_PATH = Environment
			.getExternalStorageDirectory() + "/photo/head/";

	public static final int REQUEST_CODE_INSTALL = 1001;

	public static final int REQUEST_CODE_UPDATE = 1002;

	public static final int REQUEST_CODE_UNINSTALL = 1003;

	public static final int REQUEST_CODE_OPEN = 1004;

	/**
	 * 相册
	 */
	public static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择

	public static final int PHOTO_REQUEST_CUT = 3;// 结果

}