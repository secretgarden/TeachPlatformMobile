package com.wosai.teach.utils;

import android.content.Context;
import android.graphics.Rect;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-5-12 上午10:58:36
 * @desc : 一直走马灯效果控件
 */
public class CustomMarqueeTextView extends TextView {
	// 无文字限制
	// http://cache.baiducontent.com/c?m=9f65cb4a8c8507ed4fece763105392230e54f730608190482482904a91735b361a26b6a67d644f5acec57e6406ad4941e9f42b71340826b49bdf883d87fdcd763bcd7a742613d51d449344f49d5124b137e65ffed96df0bb8025e2aec5a4a94323cd44747e97f0fa4d7163dd1ef60347e6b1ef4c025e60ad9d34728e28&p=8060c64ad4934ea45fa5e6214a51&newp=aa638f15d9c846b10cbe9b7c495292695803ed683cd1ce41&user=baidu&fm=sc&query=android%D7%DF%C2%ED%B5%C6+%CE%C4%D7%D6%B6%CC&qid=ba4e49480000844e&p1=19

	public CustomMarqueeTextView(Context context) {
		super(context);
		createView();
	}

	public CustomMarqueeTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		createView();
	}

	public CustomMarqueeTextView(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		createView();
	}

	private void createView() {
		setEllipsize(TruncateAt.MARQUEE);
		setMarqueeRepeatLimit(-1);
		setSingleLine(true);
		setFocusable(true);
		setFocusableInTouchMode(true);
	}

	@Override
	protected void onFocusChanged(boolean focused, int direction,
			Rect previouslyFocusedRect) {
		if (focused) {
			super.onFocusChanged(focused, direction, previouslyFocusedRect);
		}
	}

	@Override
	public void onWindowFocusChanged(boolean focused) {
		if (focused) {
			super.onWindowFocusChanged(focused);
		}
	}

	@Override
	public boolean isFocused() {
		return true;
	}

}