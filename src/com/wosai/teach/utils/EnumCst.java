package com.wosai.teach.utils;

public class EnumCst extends BaseCst {

	// 应用安装状态 0,不可用，1未安装，2已安装，3安装中，4更新中，9异常
	public static final int TEACH_STATUS_INVISIBLE = 0;

	public static final int TEACH_STATUS_NOT_INSTALL = 1;

	public static final int TEACH_STATUS_INSTALL = 2;

	public static final int TEACH_STATUS_INSTALLING = 3;

	public static final int TEACH_STATUS_UPDATEING = 4;

	public static final int TEACH_STATUS_DOWNLOAD = 5;

	public static final int TEACH_STATUS_LOADOVER = 6;

	public static final int TEACH_STATUS_EXCEPTION = 9;

	// 客户端操作系统类型，0：未知；1：安卓；2：IOS；3：WINDOS；4：Linux
	public static final int OS_TYPE_UNKNOW = 0;

	public static final int OS_TYPE_ANDROID = 1;

	public static final int OS_TYPE_IOS = 2;

	public static final int OS_TYPE_WINDOS = 3;

	public static final int OS_TYPE_LINUX = 4;
}
