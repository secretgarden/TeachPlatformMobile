package com.wosai.teach.utils;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.DiskCache;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.MemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.wosai.teach.R;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-28 上午11:30:52
 * @desc : 图片加载
 */
public class ImageLoaders {

	private static final String IMAGE_CACHE_PATH = "/agnetty/image/Cache";

	private static final int DEFAULT_IMAGE_RES = R.drawable.icon;

	private static ImageLoaders mLoader;

	/**
	 * 构造器
	 */
	private ImageLoaders() {
	}

	/**
	 * 获取CCImageLoader的单例
	 */
	public static ImageLoaders getInstance() {
		if (mLoader == null) {
			mLoader = new ImageLoaders();
		}
		return mLoader;
	}

	/**
	 * 初始化
	 */
	public void init(Context context) {

		File cacheDir = StorageUtils.getOwnCacheDirectory(context,
				IMAGE_CACHE_PATH);

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context).threadPriority(Thread.NORM_PRIORITY - 2)
				.memoryCache(new LruMemoryCache(2 * 1024 * 1024))
				.memoryCacheSize(2 * 1024 * 1024)
				.denyCacheImageMultipleSizesInMemory()
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.diskCache(new UnlimitedDiscCache(cacheDir))
				.tasksProcessingOrder(QueueProcessingType.FIFO).build();
		ImageLoader.getInstance().init(config);
	}

	/**
	 * 删除一条缓存
	 * 
	 * @param key
	 */
	@SuppressWarnings("deprecation")
	public static void remove(String key) {
		MemoryCache memory = ImageLoader.getInstance().getMemoryCache();
		MemoryCacheUtils.removeFromCache(key, memory);
		
		DiskCache disk = ImageLoader.getInstance().getDiskCache();
		disk.remove(key);
	}

	/**
	 * 显示指定尺寸的图片显示到ImageView中
	 */
	public void displayImage(ImageView targetView, String urlOrPath) {
		displayImage(targetView, urlOrPath, DEFAULT_IMAGE_RES);
	}

	/**
	 * 显示指定尺寸的图片显示到ImageView中<BR>
	 */
	public void displayImage(ImageView targetView, String urlOrPath,
			int defaultResId) {
		ImageLoader.getInstance().displayImage(
				urlOrPath,
				targetView,
				getDefaultDisplayImageOptions(null, defaultResId, defaultResId,
						defaultResId));
	}

	/**
	 * 获取默认的DisplayImageOptions对象<BR>
	 */
	private DisplayImageOptions getDefaultDisplayImageOptions(String imageType,
			int stubImageRes, int emptyImageRes, int failImageRes) {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				// 在显示真正的图片前，会加载这个资源
				.showImageOnLoading(stubImageRes)
				.showImageForEmptyUri(emptyImageRes)
				// 加载失败时显示该资源
				.showImageOnFail(failImageRes)
				.bitmapConfig(Bitmap.Config.RGB_565)
				// 图片的缩放方式
				.imageScaleType(ImageScaleType.EXACTLY)
				// 开启内存缓存
				.cacheInMemory(true)
				// 开启硬盘缓存
				.cacheOnDisk(true).displayer(new SimpleBitmapDisplayer())
				.build();

		return options;
	}

	public void displayRoundImage(ImageView targetView, String urlOrPath,
			int defaultResId) {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				// 在显示真正的图片前，会加载这个资源
				.showImageOnLoading(defaultResId)
				.showImageForEmptyUri(defaultResId)
				// 加载失败时显示该资源
				.showImageOnFail(defaultResId)
				.bitmapConfig(Bitmap.Config.RGB_565)
				// 图片的缩放方式
				.imageScaleType(ImageScaleType.EXACTLY)
				// 开启内存缓存
				.cacheInMemory(true)
				// 开启硬盘缓存
				.cacheOnDisk(true).displayer(new RoundBitmapDisplayer())
				.build();
		ImageLoader.getInstance().displayImage(urlOrPath, targetView, options);
	}

}
