/*
 * ========================================================
 * Copyright(c) 2012 杭州龙骞科技-版权所有
 * ========================================================
 * 本软件由杭州龙骞科技所有, 未经书面许可, 任何单位和个人不得以
 * 任何形式复制代码的部分或全部, 并以任何形式传播。
 * 公司网址
 * 
 * 			http://www.hzdracom.com/
 * 
 * ========================================================
 */

package com.wosai.teach.utils;

import android.util.SparseIntArray;

import com.wosai.teach.handler.InstallHandler;
import com.wosai.teach.pojo.User;
import com.wosai.teach.thread.DownloadFactory;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-28 上午11:37:45
 * @desc : 全局变量
 */
public class AppGlobal {

	// 是否存在同学数据
	public static boolean hasSchoolMate = false;

	// 找一找结果，总页数
	public static int fsPageCount;

	// 同学圈，总页数
	public static int circlePageCount;

	// 用户信息
	public static User user;

	public static User lastUser;

	public static int curInstExpId = 0;

	public static DownloadFactory factory = null;

	public static InstallHandler instHandler;
}
