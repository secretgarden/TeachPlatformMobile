package com.wosai.teach.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedSession {

	/**
	 * 是否第一次使用
	 * 
	 * @param context
	 * @return
	 */
	public static boolean firstUse(Context context) {
		SharedPreferences sp = context.getSharedPreferences("user",
				Context.MODE_PRIVATE);
		Editor et = sp.edit();
		boolean result = sp.getBoolean("firstUse", true);
		if (result) {
			et.putBoolean("firstUse", false);
			et.commit();
		}
		return result;
	}

	/**
	 * 保存数据到缓存
	 * 
	 * @param context
	 * @param name
	 * @param value
	 */
	public static void save(Context context, String name, String value) {
		SharedPreferences sp = context.getSharedPreferences("cache",
				Context.MODE_PRIVATE);
		Editor et = sp.edit();
		et.putString(name, value);
		et.commit();
	}

	/**
	 * 缓存中读取数据
	 * 
	 * @param context
	 * @param name
	 * @return
	 */
	public static String get(Context context, String name) {
		SharedPreferences sp = context.getSharedPreferences("cache",
				Context.MODE_PRIVATE);
		String result = sp.getString(name, "");
		return result;
	}

	/**
	 * 用户能否使用功能
	 * 
	 * @param context
	 * @return
	 */
	public static boolean UserCanUse(Context context, String userCode) {
		SharedPreferences sp = context.getSharedPreferences("user",
				Context.MODE_PRIVATE);
		boolean result = sp.getBoolean("canUse_" + userCode, false);
		return result;
	}
}
