package com.wosai.teach.utils;

import android.os.Build;

public class BaseCst {
	// 操作成功
	public static final int HTTP_CODE_SUCCESS = 100;
	// 操作失败（如没有明确的错误原因，则返回此错误码）
	public static final int HTTP_CODE_FAILURE = 101;
	// 升级接口，已是最新版本
	public static final int HTTP_CODE_UPDATE = 104;
	// 兑换、余额不足
	public static final int HTTP_CODE_SCORE = 500;
	// 兑换类型不支持
	public static final int HTTP_CODE_TRADETYPE = 501;

	public final static String FIELD_CMD = "cmd";
	public final static String FIELD_OSTYPE = "osType";
	public final static String FIELD_DEVICE = "device";
	public final static String FIELD_DEVICETYPE = "deviceType";
	public final static String FIELD_SDKCHANNEL = "sdkChannel";
	public final static String FIELD_APPCHANNEL = "appChannel";
	public final static String FIELD_USERID = "user_id";
	public final static String FIELD_STYPE = "s_type";
	public final static String FIELD_USERMOBILE = "user_mobile";
	public final static String FIELD_MSGKEY = "msg_key";
	public final static String FIELD_APPID = "appId";
	public final static String FIELD_TIME = "time";
	public final static String FIELD_BODY = "body";
	public final static String FIELD_TOKEN = "token";
	public final static String FIELD_CODE = "code";
	public final static String FIELD_APIKEY = "apiKey";
	public final static String FIELD_START = "start";
	public final static String FIELD_SIZE = "size";
	public final static String FIELD_MOTHED = "method";
	public final static String FIELD_XM = "s_xm";
	public final static String FIELD_SMOBILE = "s_mobile";
	
	public final static String FIELD_VERSION         	 = "version";
	public final static String FIELD_TYPE         		 = "type";
	public final static String FIELD_URL         		 = "url";
	public final static String FIELD_DESC         		 = "desc";
	
	public final static String FIELD_LOGINID             = "loginID";         // 账号名，等同于user里的username，区别与driverID
	public final static String FIELD_USERNAME            = "username";        // 用户姓名，等同于user里的nickname
	public final static String FIELD_PASSWORD            = "password";        // 密码
	public final static String FIELD_DRIVERID            = "driverId";        // 车主表编号
	public final static String FIELD_PARKID              = "parkID";          
	public final static String FIELD_UUID                = "UUID";            // 蓝牙模块UUID
	public final static String FIELD_RESERVEID           = "appointmentID";   // 预约表ID
	public final static String FIELD_LONGITUDE           = "localLongitude";  // 经度
	public final static String FIELD_LATIUDE             = "locaLatitude";    // 维度
	public final static String FIELD_PHONENUMBER         = "phoneNumber";     // 电话号码
	public final static String FIELD_PLATENUMBER         = "plateNumber";     // 车牌号
	public final static String FIELD_PHATENUMBER         = "phateNumber";     // 服务端写错的车牌号
	public final static String FIELD_FEEDBACK            = "content";
	public final static String FIELD_PLATENUMBERID       = "plateNumberID";
	public final static String FIELD_PAGE                = "page";            //获取停车数据时的页数
	public final static String FIELD_MESSAGE_ID          = "message_id";
	public final static String FIELD_ACTION_ID           = "action_id";
	
	public final static String FIELD_TITLE         	 	 = "title";
	
	public final static String FIELD_DATA0        	 	 = "data0";
	public final static String FIELD_DATA1        	 	 = "data1";
	public final static String FIELD_DATA2        	 	 = "data2";
	public final static String FIELD_DATA3        	 	 = "data3";
	public final static String FIELD_DATA4        	 	 = "data4";
	public final static String FIELD_DATA5        	 	 = "data5";
	public final static String FIELD_DATA6       	 	 = "data6";
	public final static String FIELD_DATA7        	 	 = "data7";
	
	public final static String FIELD_BAIDUCHANNELID      = "baiduChannelId";
	public final static String FIELD_BAIDUUSERID         = "baiduUserId";

	public final static String FIELD_CONTENT     	 	 = "content";
	public final static String FIELD_ICONURL     	 	 = "iconUrl";
	
	public final static String VALUE_OSTYPE         	 = "android";
	public final static String VALUE_DEVICE         	 = Build.MANUFACTURER+" "+Build.MODEL+"/"+Build.VERSION.RELEASE;
	public final static String VALUE_DEVICE_PAD          = "pad";
	public final static String VALUE_DEVICE_PHONE        = "phone";
	
	public final static String VALUE_CMD_REGISTER      	 = "driverRegister";
	public final static String VALUE_CMD_LOGIN           = "driverLogin";
	public final static String VALUE_CMD_GETPARKER       = "getvicinityparks";
	public final static String VALUE_CMD_UPDATEPUSH      = "updateDriverPushInfo";
	public final static String VALUE_CMD_RESERVER_PARKER = "MakeAnAppointmentRequest";
	public final static String VALUE_CMD_CANCEL_RESERVER = "CancelAnAppointmentRequest";
	public final static String VALUE_CMD_FEEDBACK        = "sendDriverFeedback";
	public final static String VALUE_CMD_PERSONALINFO    = "updateDriverInfodetail";
	public final static String VALUE_CMD_ADDPLATENUMBER  = "addPlateNumber";
	public final static String VALUE_CMD_DELETEPLATENUMBER       = "deletePlateNumber";
	public final static String VALUE_CMD_SETDEFAULTPLATENUMBER   = "setDefaultPlateNumber";
	public final static String VALUE_CMD_GETPARKINGDATA          = "getParkingRecoderListByDriverID";
	public final static String VALUE_CMD_INOUTPARKMSGBYIBEACON   = "InOutParkMsgByIbeacon";
}




