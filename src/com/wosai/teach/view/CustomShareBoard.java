/**
 * 
 */

package com.wosai.teach.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.bean.SocializeEntity;
import com.umeng.socialize.bean.StatusCode;
import com.umeng.socialize.controller.UMServiceFactory;
import com.umeng.socialize.controller.UMSocialService;
import com.umeng.socialize.controller.listener.SocializeListeners.SnsPostListener;
import com.umeng.socialize.media.QQShareContent;
import com.umeng.socialize.media.QZoneShareContent;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.sso.QZoneSsoHandler;
import com.umeng.socialize.sso.UMQQSsoHandler;
import com.umeng.socialize.weixin.controller.UMWXHandler;
import com.umeng.socialize.weixin.media.CircleShareContent;
import com.umeng.socialize.weixin.media.WeiXinShareContent;
import com.wosai.teach.R;
import com.wosai.teach.utils.C;

/**
 * 
 */
public class CustomShareBoard extends PopupWindow implements OnClickListener {

	String sign = "82b2f76b0c4f487b4f79f1dcdd5a35d0";

	String QQ_APP_ID = "1104707544";
	String QQ_APP_KEY = "tl1jrtWd78jLEsnH";

	// String WX_APP_ID = "wx39b231db714c98e5";
	// String WX_APP_SECRET = "a6e082a2ddfbda6d6612c2a478a9d85c";

	String WX_APP_ID = "wxf7505cd19fee0c8c";
	String WX_APP_SECRET = "53eba3d24aeaf9f1347a446a56a765d0";

	private UMSocialService mController = UMServiceFactory
			.getUMSocialService(C.DESCRIPTOR);
	private Activity mActivity;

	public CustomShareBoard(Activity activity) {
		super(activity);
		this.mActivity = activity;
		initView(activity);
		// mController.getConfig().closeToast();
		// mController.getConfig().openToast();
		this.loadWXShare();
		this.loadQQShare();
	}

	@SuppressWarnings("deprecation")
	private void initView(Context context) {
		View rootView = LayoutInflater.from(context).inflate(
				R.layout.custom_board, null);
		rootView.findViewById(R.id.wechat).setOnClickListener(this);
		rootView.findViewById(R.id.wechat_circle).setOnClickListener(this);
		rootView.findViewById(R.id.qq).setOnClickListener(this);
		rootView.findViewById(R.id.qzone).setOnClickListener(this);
		setContentView(rootView);
		setWidth(LayoutParams.MATCH_PARENT);
		setHeight(LayoutParams.WRAP_CONTENT);
		setFocusable(true);
		setBackgroundDrawable(new BitmapDrawable());
		setTouchable(true);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch (id) {
		case R.id.wechat:
			performShare(SHARE_MEDIA.WEIXIN);
			break;
		case R.id.wechat_circle:
			performShare(SHARE_MEDIA.WEIXIN_CIRCLE);
			break;
		case R.id.qq:
			performShare(SHARE_MEDIA.QQ);
			break;
		case R.id.qzone:
			performShare(SHARE_MEDIA.QZONE);
			break;
		default:
			break;
		}
	}

	private void performShare(SHARE_MEDIA platform) {
		mController.postShare(mActivity, platform, new SnsPostListener() {

			@Override
			public void onStart() {

			}

			@Override
			public void onComplete(SHARE_MEDIA platform, int eCode,
					SocializeEntity entity) {
				String showText = platform.toString();
				if (eCode == StatusCode.ST_CODE_SUCCESSED) {
					showText += "平台分享成功";
				} else {
					showText += "平台分享失败";
				}
				System.out.println(eCode + "|" + showText);
				Toast.makeText(mActivity, showText, Toast.LENGTH_SHORT).show();
				dismiss();
			}
		});
	}

	public void loadQQShare() {
		// 参数1为当前Activity， 参数2为开发者在QQ互联申请的APP ID，参数3为开发者在QQ互联申请的APP kEY.
		UMQQSsoHandler qqSsoHandler = new UMQQSsoHandler(mActivity, QQ_APP_ID,
				QQ_APP_KEY);
		qqSsoHandler.addToSocialSDK();

		QQShareContent qqShareContent = new QQShareContent();
		// 设置分享文字
		qqShareContent.setShareContent("学习好伙伴，工作好助手");
		// 设置分享title
		qqShareContent.setTitle("仿真教育平台");
		// 设置分享图片
		qqShareContent.setShareImage(new UMImage(mActivity, R.drawable.icon));
		// 设置点击分享内容的跳转链接
		qqShareContent.setTargetUrl("http://wosaitech.com/");
		mController.setShareMedia(qqShareContent);

		// 参数1为当前Activity， 参数2为开发者在QQ互联申请的APP ID，参数3为开发者在QQ互联申请的APP kEY.
		QZoneSsoHandler qZoneSsoHandler = new QZoneSsoHandler(mActivity,
				QQ_APP_ID, QQ_APP_KEY);
		qZoneSsoHandler.addToSocialSDK();

		QZoneShareContent qzone = new QZoneShareContent();
		// 设置分享文字
		qzone.setShareContent("来自友盟社会化组件（SDK）让移动应用快速整合社交分享功能 -- QZone");
		// 设置点击消息的跳转URL
		qzone.setTargetUrl("你的URL链接");
		// 设置分享内容的标题
		qzone.setTitle("QZone title");
		// 设置分享图片
		// qzone.setShareImage(urlImage);
		mController.setShareMedia(qzone);
	}

	public void loadWXShare() {
		// 设置分享内容
		mController
				.setShareContent("友盟社会化组件（SDK）让移动应用快速整合社交分享功能，http://www.umeng.com/social");
		// 设置分享图片, 参数2为图片的url地址
		mController.setShareMedia(new UMImage(mActivity,
				"http://www.umeng.com/images/pic/banner_module_social.png"));

		// 添加微信平台
		UMWXHandler wxHandler = new UMWXHandler(mActivity, WX_APP_ID,
				WX_APP_SECRET);
		wxHandler.addToSocialSDK();
		// 支持微信朋友圈
		UMWXHandler wxCircleHandler = new UMWXHandler(mActivity, WX_APP_ID,
				WX_APP_SECRET);
		wxCircleHandler.setToCircle(true);
		wxCircleHandler.addToSocialSDK();

		// 设置微信好友分享内容
		WeiXinShareContent weixinContent = new WeiXinShareContent();
		// 设置分享文字
		weixinContent.setShareContent("来自友盟社会化组件（SDK）让移动应用快速整合社交分享功能，微信");
		// 设置title
		weixinContent.setTitle("友盟社会化分享组件-微信");
		// 设置分享内容跳转URL
		weixinContent.setTargetUrl("你的URL链接");
		// 设置分享图片
		// weixinContent.setShareImage(localImage);
		mController.setShareMedia(weixinContent);

		// 设置微信朋友圈分享内容
		CircleShareContent circleMedia = new CircleShareContent();
		circleMedia.setShareContent("来自友盟社会化组件（SDK）让移动应用快速整合社交分享功能，朋友圈");
		// 设置朋友圈title
		circleMedia.setTitle("友盟社会化分享组件-朋友圈");
		// circleMedia.setShareImage(localImage);
		circleMedia.setTargetUrl("你的URL链接");
		mController.setShareMedia(circleMedia);

		SnsPostListener mSnsPostListener = new SnsPostListener() {
			@Override
			public void onStart() {

			}

			@Override
			public void onComplete(SHARE_MEDIA platform, int stCode,
					SocializeEntity entity) {
				if (stCode == 200) {
					Toast.makeText(mActivity, "分享成功", Toast.LENGTH_SHORT)
							.show();
				} else {
					Toast.makeText(mActivity, "分享失败 : error code : " + stCode,
							Toast.LENGTH_SHORT).show();
				}
				System.out.println(stCode + "|" + "  |");
			}
		};
		mController.registerListener(mSnsPostListener);
	}
}
