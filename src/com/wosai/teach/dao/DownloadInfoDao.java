package com.wosai.teach.dao;

import com.activeandroid.query.Select;
import com.wosai.teach.pojo.DownloadInfo;

public class DownloadInfoDao extends BaseDao {

	public DownloadInfoDao() {
		this.initForm();
	}

	public void initForm() {
		from = new Select().from(DownloadInfo.class);
	}

	public DownloadInfo getDownloadInfoByExpId(Integer expId) {
		this.initForm();
		from.where("expId = ?", expId);
		DownloadInfo downloadInfo = from.executeSingle();
		return downloadInfo;
	}
}
