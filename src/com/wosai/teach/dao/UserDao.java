/*
 * ========================================================
 * Copyright(c) 2012 杭州龙骞科技-版权所有
 * ========================================================
 * 本软件由杭州龙骞科技所有, 未经书面许可, 任何单位和个人不得以
 * 任何形式复制代码的部分或全部, 并以任何形式传播。
 * 公司网址
 * 
 * 			http://www.hzdracom.com/
 * 
 * ========================================================
 */
package com.wosai.teach.dao;

import java.util.ArrayList;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.wosai.teach.pojo.Install;
import com.wosai.teach.pojo.User;

/**
 * @author : qiumy
 * @e-mail : qiumy@wosaitech.com
 * @date : 2015-3-28 下午2:14:12
 * @desc :
 */
public class UserDao extends BaseDao {

	public UserDao() {
		this.initForm();
	}

	public void initForm() {
		from = new Select().from(User.class);
	}

	/**
	 * 获取用户信息
	 * 
	 * @return
	 */
	public User getUserByName(String name) {
		from.where("nickName = ?", name);
		User person = from.executeSingle();
		return person;
	}

	public User getUserByLoginName(String loginName) {
		from.where("loginName = ?", loginName);
		User person = from.executeSingle();
		return person;
	}

	public void saveOrUpdateUser(User user) {
		User dbUser = this.getUserByLoginName(user.getLoginName());
		if (dbUser != null) {
			user.corpModel(dbUser);
		}
		this.initForm();
		this.save(user);
	}

	public List<User> findAllUser() {
		List<User> personalList = new ArrayList<User>();
		List<Model> modelList = findAll();
		if (modelList == null || modelList.isEmpty()) {
			return null;
		}
		for (Model model : modelList) {
			User user = (User) model;
			personalList.add(user);
		}
		return personalList;
	}

	/**
	 * 是否有用户
	 * 
	 * @return
	 */
	public boolean hasPersonal(User user) {
		from.where("nickName = ?", user.getNickName());
		User person = from.executeSingle();
		if (person == null) {
			return false;
		} else {
			return true;
		}
	}

}
