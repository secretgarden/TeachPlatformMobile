package com.wosai.teach.dao;

import java.util.ArrayList;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.wosai.teach.pojo.ExpRecUser;

public class ExpRecUserDao extends BaseDao {

	public ExpRecUserDao() {
		this.initForm();
	}

	public void initForm() {
		from = new Select().from(ExpRecUser.class);
	}

	public void saveOrUpdate(ExpRecUser experiment) {
		ExpRecUser db_exp = this.getExperimentByRecId(experiment.getRecId());
		if (db_exp == null || db_exp.getExpId() != experiment.getExpId()) {
			experiment.save();
		}
	}

	public ExpRecUser getExperimentByRecId(Integer recId) {
		this.initForm();
		from.where("recId = ?", recId);
		ExpRecUser experiment = from.executeSingle();
		return experiment;
	}

	public List<ExpRecUser> findAllExpRecUser() {
		List<ExpRecUser> allList = new ArrayList<ExpRecUser>();
		List<Model> modelList = findAll();
		if (modelList == null || modelList.isEmpty()) {
			return null;
		}
		for (Model model : modelList) {
			ExpRecUser all = (ExpRecUser) model;
			allList.add(all);
		}
		return allList;
	}

}
