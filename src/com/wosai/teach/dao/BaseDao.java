package com.wosai.teach.dao;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.query.From;

public class BaseDao {

	protected From from;

	public void initForm() {

	}

	public void save(Model model) {
		model.save();
	}

	public void delete(Model model) {
		model.delete();
	}

	public List<Model> findAll() {
		this.initForm();
		List<Model> modelList = from.execute();
		return modelList;
	}
}
