package com.wosai.teach.dao;

import java.util.ArrayList;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.query.Select;
import com.wosai.teach.pojo.ExperimentRec;

public class ExperimentRecDao extends BaseDao {

	public ExperimentRecDao() {
		this.initForm();
	}

	public void initForm() {
		from = new Select().from(ExperimentRec.class);
	}

	public void saveOrUpdate(ExperimentRec experiment) {
		ExperimentRec db_exp = this.getExperimentByExpId(experiment.getExpId());
		if (db_exp == null || db_exp.getExpId() != experiment.getExpId()) {
			experiment.save();
		}
	}

	public ExperimentRec getExperimentByExpId(Integer expId) {
		this.initForm();
		from.where("expId = ?", expId);
		ExperimentRec experiment = from.executeSingle();
		return experiment;
	}

	public List<ExperimentRec> findAllExperimentRec() {
		List<ExperimentRec> allList = new ArrayList<ExperimentRec>();
		List<Model> modelList = findAll();
		if (modelList == null || modelList.isEmpty()) {
			return null;
		}
		for (Model model : modelList) {
			ExperimentRec all = (ExperimentRec) model;
			allList.add(all);
		}
		return allList;
	}

}
