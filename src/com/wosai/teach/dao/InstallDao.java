package com.wosai.teach.dao;

import com.activeandroid.query.Select;
import com.wosai.teach.pojo.Install;

public class InstallDao extends BaseDao {

	public InstallDao() {
		this.initForm();
	}

	public void initForm() {
		from = new Select().from(Install.class);
	}

	public Install getInstallByExpId(Integer expId) {
		this.initForm();
		from.where("expId = ?", expId);
		Install install = from.executeSingle();
		return install;
	}
}
